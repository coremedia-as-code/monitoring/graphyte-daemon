#!/usr/bin/env python3

import configuration
import graphyte

import traceback

import os
import sys
import re
import platform

import json
import threading
import time
import logging

from http.server import HTTPServer, BaseHTTPRequestHandler

from pathlib import Path
from datetime import datetime

import enum

VERSION = '1.2.1'


class Logging(enum.Enum):
  DEBUG    = 10 # getattr(logging, 'DEBUG')
  NOTICE   = 15 #getattr(logging, 'NOTICE')
  INFO     = getattr(logging, 'INFO')
  WARNING  = getattr(logging, 'WARNING')
  ERROR    = getattr(logging, 'ERROR')
  CRITICAL = getattr(logging, 'CRITICAL')


logging.basicConfig(level=logging.INFO, format='(%(threadName)-26s) %(message)s',)


class Collector(threading.Thread):
    """

    """
    def __init__(self, group=None, target=None, graphyte=None, name=None, instance_name=None, args=(), kwargs=None, log_level = Logging.NOTICE ):
        """
        """
        threading.Thread.__init__( self, group = group, target = target, name = name)
        self.running        = True
        self.name           = name
        self.instance_name  = instance_name
        self.args           = args
        self.kwargs         = kwargs
        self.graphyte       = graphyte
        self.log_level      = log_level

        self.log            = logging.getLogger('graphyte-daemon')
        streamhandler       = logging.StreamHandler(sys.stdout)

        # self.log.addHandler(streamhandler)
        # self.log.setLevel(logging.DEBUG)

        return


    def send_to_graphite(self, data):
        """

        """
        # return

        if isinstance(data, list):

            try:
                for dic in data:

                    if isinstance(dic, dict):

                        graphite_key   = dic.get("key", None)

                        if( graphite_key == None ):
                            self.log.warning(" no data found")
                            self.log.debug(" {}".format(dic))
                        else:
                            graphite_value     = dic.get("value", 0.0)
                            graphite_timestamp = dic.get("timestamp", None)
                            # print( " %s is %s (%s)" % ( graphite_key, graphite_value, graphite_timestamp ) )

                            self.graphyte.send(
                              "%s.%s" % ( self.instance_name, graphite_key ),
                              graphite_value,
                              graphite_timestamp
                            )

            except Exception as exc:
                print("ERROR: failed to dispatch values :: %s :: %s" % (exc, traceback.format_exc()))


    def run(self):
        """

        """
        self.log.debug('running with %s', self.kwargs)

        args    = self.kwargs
        plugins = args['plugins']
        counter = 0;

        args.pop('plugins')

        while self.running:
            """
            """
            current_datetime = datetime.now()
            current_datetime.strftime( "%d.%m.%Y %H:%M:%S" )

            for plugin in plugins:

                file_name = "plugins/%s.py" % (plugin)

                if( not os.path.exists(file_name) ):
                    self.log.error("file : %s not exists" % (file_name) )
                    continue

                p = None

                try:
                    if(self.log_level != 'NOTICE'):
                        print( "%s :: %-26s :: %s" % ( current_datetime.strftime( "%d.%m.%Y %H:%M:%S" ), self.name, plugin ) )

                    if(plugin == 'operatingsystem'):

                        try:
                            from plugins.operatingsystem import OperatingSystem
                            p = OperatingSystem(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    if(plugin == 'nginx'):

                        try:
                            from plugins.nginx import Nginx
                            p = Nginx(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'mysql'):

                        try:
                            from plugins.mysql import Mysql
                            p = Mysql(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'postgres'):

                        try:
                            from plugins.postgres import Postgres
                            p = Postgres(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'mongodb'):

                        try:
                            from plugins.mongodb import Mongodb
                            p = Mongodb(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'java'):

                        try:
                            from plugins.java import Java
                            p = Java(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'tomcat'):

                        try:
                            from plugins.tomcat import Tomcat
                            p = Tomcat(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    if(plugin == 'garbage_collector'):

                        try:
                            from plugins.garbage_collector import GarbageCollector
                            p = GarbageCollector(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    if(plugin == 'solr'):

                        try:
                            from plugins.solr import Solr
                            p = Solr(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'license'):

                        try:
                            from plugins.license import License
                            p = License(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'content_server'):

                        try:
                            from plugins.content_server import ContentServer
                            p = ContentServer(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'cap_connection'):

                        try:
                            from plugins.cap_connection import CapConnection
                            p = CapConnection(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'publisher'):

                        try:
                            from plugins.publisher import Publisher
                            p = Publisher(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'query_pool'):

                        try:
                            from plugins.query_pool import QueryPool
                            p = QueryPool(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'connection_pool'):

                        try:
                            from plugins.connection_pool import ConnectionPool
                            p = ConnectionPool(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'sequence_number'):

                        try:
                            from plugins.sequence_number import SequenceNumber
                            p = SequenceNumber(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'cae_feeder'):

                        try:
                            from plugins.cae_feeder import CAEFeeder
                            p = CAEFeeder(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'content_feeder'):

                        try:
                            from plugins.content_feeder import ContentFeeder
                            p = ContentFeeder(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'cache_classes'):

                        try:
                            from plugins.cache_classes import CacheClasses
                            p = CacheClasses(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'memory_pool'):

                        try:
                            from plugins.memory_pool import MemoryPool
                            p = MemoryPool(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'statistics'):

                        try:
                            from plugins.statistics import Statistics
                            p = Statistics(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                    elif(plugin == 'activemq'):

                        try:
                            from plugins.activemq import ActiveMQ
                            p = ActiveMQ(**args)

                        except Exception as exc:
                            self.log.error("Collector: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

                except  Exception as e:
                    self.log.error( "{}".format(e) )

                if(p == None):
                  # print("DEBUG: no class defined")
                  continue

                data = p.get_stats()

                self.send_to_graphite(data)

                if(counter == 12):
                    print('-- MARK ALIVE --')
                    counter = -1

            # TODO
            # make it configurable
            time.sleep(10)

            counter+= 1

        return


    def stop(self):
        self.running = False


class ThreadWatch(threading.Thread):
    """
    """
    def __init__(self, count ):

        threading.Thread.__init__( self, name = 'ThreadWatch' )
        self.running      = True
        self.count        = count
        self.log          = logging.getLogger('ThreadWatch')

        streamhandler     = logging.StreamHandler(sys.stdout)

        return


    def run(self):
        """

        """
        self.log.debug('watch %d threads', self.count)
        time.sleep(20)

        master_threads = 0
        thread_blacklist =  ['MainThread', 'ThreadWatch', 'RestInterface', 'Thread-1', 'pymongo_server_monitor_thread', 'pymongo_kill_cursors_thread']

        for thread in threading.enumerate():
            if(thread.name in thread_blacklist or thread.name.startswith('tail-')):
                master_threads += 1

        while self.running:

            thread_names = []
            for thread in threading.enumerate():
                if(thread.name in thread_blacklist or thread.name.startswith('tail-')):
                    continue
                thread_names.append(thread.name)

            # print(thread_names)
            # minus MainThread, ThreadWatch, RestInterface, graphite
            found_threads = threading.activeCount() - master_threads

            current_datetime = datetime.now()
            current_datetime.strftime( "%d.%m.%Y %H:%M:%S" )
            self.log.info( "%s :: %-26s :: %d threads of %d found (%s)" % ( current_datetime.strftime( "%d.%m.%Y %H:%M:%S" ), self.name, found_threads, self.count, thread_names ) )
            # print( "%s :: %-26s :: %d threads of %d found (%s)" % ( current_datetime.strftime( "%d.%m.%Y %H:%M:%S" ), self.name, found_threads, self.count, thread_names ) )

            if( found_threads < self.count ):
                missing = int(self.count) - int(found_threads)
                self.log.error( "%s :: %-26s :: %d thread(s are) missing, there should be %d" % ( current_datetime.strftime( "%d.%m.%Y %H:%M:%S" ), self.name, missing, self.count ) )
                os._exit(127)

            # TODO
            # make it configurable
            time.sleep(30)


    def stop(self):
        self.running = False

# Defining a HTTP request Handler class
class ServiceHandler(BaseHTTPRequestHandler):
    """

    """
    def log_message(self, format, *args):
        return

    # GET Method Defination
    def do_GET(self):
        # defining all the headers
        request = self.path
        prog    = re.compile("/healthcheck$")
        result  = prog.match(request)

        if not result :
          self.send_response(503)
        else:

          self.send_response(200)
          self.send_header('Content-type','text/json')
          self.end_headers()

          data = {
            "status": 200,
            "version": VERSION
          }

          self.wfile.write(json.dumps(data).encode())


    def do_HEAD(self):
        # expect request healthcheck_PORT
        request = self.path
        prog    = re.compile("/healthcheck/\d{1,5}$")
        result  = prog.match(request)

        if not result :
          self.send_response(503)

        # else :
        #     testport = request.split("_")[1]
        #     try:
        #         telnetlib.Telnet(host="localhost", port=testport, timeout=3)
        #         self.send_response(200)
        #     except Exception, e:
        #         self.send_response(503)
        #     finally:
        #         self.end_headers()
        #
        # return None


class RestInterface(threading.Thread):
    """
    """
    def __init__(self, port = 8888, services = {} ):

        threading.Thread.__init__( self, name = 'RestInterface' )
        self.running      = True
        self.log          = logging.getLogger('RestInterface')
        self._port        = port
        self._services    = services

    def run(self):
        logging.basicConfig(level=logging.ERROR)
        server = HTTPServer(('127.0.0.1', self._port), ServiceHandler)
        server.serve_forever()

    def stop(self):
        self.running = False


class NginxLogfiles(threading.Thread):
    """

    """
    def __init__(self, group=None, target=None, graphyte=None, name=None, instance_name=None, args=(), kwargs=None, log_level = Logging.NOTICE ):
        """

        """
        threading.Thread.__init__( self, group = group, target = target, name = name )
        self.running        = True
        self.name           = name
        self.instance_name  = instance_name
        self.args           = args
        self.kwargs         = kwargs
        self.graphyte       = graphyte
        self.log_level      = log_level

        self.log            = logging.getLogger('NginxLogfiles')
        # self.log.setLevel(logging.DEBUG)
        # print(kwargs)

        self.no_follow      = False
        self.nginx_logpath  = kwargs.get("nginx_logpath", "/var/log/nginx" )
        self.nginx_logfiles = kwargs.get("nginx_logfiles", ["access.log"] )

        self.line_nginx_full = re.compile(r"""(?P<ipaddress>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) - - \[(?P<dateandtime>\d{2}\/[a-z]{3}\/\d{4}:\d{2}:\d{2}:\d{2} (\+|\-)\d{4})\] ((\"(GET|POST) )(?P<url>.+)(http\/1\.1")) (?P<statuscode>\d{3}) (?P<bytessent>\d+) (["](?P<refferer>(\-)|(.+))["]) (["](?P<useragent>.+)["])""", re.IGNORECASE)
        # self.line_nginx_onlyStatus = re.compile(r'.+HTTP\/1\.1" (?P<statuscode>\d{3})')
        # self.line_nginx_codeByMethod = re.compile(r'.+GET (?P<url>\/(?P<method>.+?)\?.+).+HTTP\/1\.1" (?P<statuscode>\d{3})')

        self.time_format    = "%d/%b/%Y:%H:%M:%S +0200"

        self.thread_lock    = threading.Lock()


    def __follow(self, the_file):
        """
          Follow a given file and yield new lines when they are available, like `tail -f`.
        """


        with open(the_file) as f:
            f.seek(0, 2)  # seek to eof
            while True:
                line = f.readline()
                if not line:
                    time.sleep(0.1)  # sleep briefly before trying again
                    continue
                yield line


    def send_to_graphite(self, data):
        """

        """
        # return

        if isinstance(data, list):

            try:
                for dic in data:

                    if isinstance(dic, dict):

                        graphite_key   = dic.get("key", None)

                        if( graphite_key == None ):
                            self.log.warning(" no data found")
                            self.log.debug(" {}".format(dic))
                        else:
                            graphite_value     = dic.get("value", 0.0)
                            graphite_timestamp = dic.get("timestamp", None)
                            # print( " %s is %s (%s)" % ( graphite_key, graphite_value, graphite_timestamp ) )

                            self.graphyte.send(
                              "%s.%s" % ( self.instance_name, graphite_key ),
                              graphite_value,
                              timestamp = graphite_timestamp
                            )

            except Exception as exc:
                print("ERROR: failed to dispatch values :: %s :: %s" % (exc, traceback.format_exc()))


    def printFile(self, logfile):
        """

        """
        result = []

        if(self.no_follow):
            loglines = open(logfile)
        else:
            loglines = self.__follow(logfile)

        logfile_name = logfile.replace(self.nginx_logpath, '').replace('.log','').replace('-','_').replace('/','')

        for l in loglines:
            #only one thread at a time can print to the user
            self.thread_lock.acquire()

            m = self.line_nginx_full.match(l)
            if m is not None:
                r = m.groupdict()

                date_time = r.get('dateandtime')
                status    = r.get('statuscode')
                bytessent = r.get('bytessent')
                useragent = r.get('useragent')

                if(useragent.startswith('check_http')): # or 'nagios-plugins' in useragent or 'monitoring-plugins' in useragent):
                    continue

                format_date = datetime.strptime(date_time, self.time_format)

                result.append({
                  "key": ".".join( [ "NGINX", 'logfile', logfile_name, "status_code", status ]),
                  "value": int(1),
                  "timestamp": format_date.timestamp()
                })
                result.append({
                  "key": ".".join( [ "NGINX", 'logfile', logfile_name, "bytes_sent" ]),
                  "value": float(bytessent),
                  "timestamp": format_date.timestamp()
                })

            self.thread_lock.release()
            self.send_to_graphite(result)


    def __parse_logfile(self, log_file):
        """

        """
        time_format = "%d/%b/%Y:%H:%M:%S +0200"

        lines = open(log_file)
        for l in lines:
            # print(l)
            m = self.line_nginx_full.match(l)
            if m is not None:
                # print(m)
                r = m.groupdict()
                # print(r)

                date_time = r.get('dateandtime')
                status    = r.get('statuscode')
                bytessent = r.get('bytessent')

                print("{} - {} - {}".format(date_time,status,bytessent))

                format_date = datetime.strptime(date_time, time_format)
                print(format_date.timestamp())

                print( time.mktime(datetime.strptime(date_time, time_format).timetuple()) )


    def run(self):
        """

        """
        for f in self.nginx_logfiles:

            current_datetime = datetime.now()
            current_datetime.strftime( "%d.%m.%Y %H:%M:%S" )

            full_file = "{}/{}".format(self.nginx_logpath, f)

            if(self.log_level != 'NOTICE'):
                print( "%s :: %-26s :: logfile %s" % ( current_datetime.strftime( "%d.%m.%Y %H:%M:%S" ), 'nginx', full_file ) )

            if not os.path.exists(full_file):
                continue

            t = threading.Thread(name = "tail-{}".format(f), target = self.printFile,args = (full_file,))
            t.start()

            # self.printFile(full_file)
            # self.__parse_logfile(full_file)


    def stop(self):
        self.running = False



if __name__ == '__main__':
    """

    """
    node_name   = platform.node()

    file_name   = Path(os.path.basename(__file__))
    config_name = file_name.with_suffix('.yml')
    cfg_file    = None
    config      = configuration.Configuration()

    for path in [ "/etc", Path.home(), Path.cwd() ]:
          cfg_file = str(path) + "/" + str(config_name)
          if( os.path.isfile( cfg_file ) ):
              break;

    if(cfg_file == None):
        raise("ERROR: no config file found")

    yml_config        = config.read_config( cfg_file )

    graphite_config   = yml_config.get('graphite', {})
    graphite_host     = None
    graphite_interval = None
    services          = []
    service_count     = 0

    thread_watch_enabled     = yml_config.get('thread_watch', True)
    rest_healthcheck_enabled = yml_config.get('healthcheck', {}).get('enabled', False)
    rest_healthcheck_port    = yml_config.get('healthcheck', {}).get('port', 8888)
    log_level                = yml_config.get('log_level', 'NOTICE')

    if(graphite_config):
        try:
            graphite_host     = graphite_config.get('host')
            graphite_interval = graphite_config.get('interval', 20)
            instance_name     = graphite_config.get('instance_name', node_name.split('.')[0])

            if( graphite_host == None ):
                raise Exception("ERROR: Configuration Error. No graphite host configuration found")

        except Exception as exc:
            print("ERROR: %s :: %s" % (exc, traceback.format_exc()))

        try:
            graphite_host
        except NameError:
            raise "ERROR: Configuration Error. No graphite host configuration found"

        try:
            # host, port=2003, prefix=None, timeout=5, interval=None, queue_size=None, log_sends=False, protocol='tcp', batch_size=1000):
            graphyte = graphyte.Sender( graphite_host, prefix = 'graphyte', interval=10 , log_sends=False )

        except Exception as exc:
            print("ERROR: %s :: %s" % (exc, traceback.format_exc()))

        services      = yml_config.get('services', [])
        service_count = len(services)

    print(' ,-----------------------------------------------------------')
    print(' |  graphyte-daemon  - version {}'.format(VERSION) )
    print(' |  save measurement points into graphite {}'.format(graphite_host) )
    print(' `-----------------------------------------------------------')

    if(rest_healthcheck_enabled):
        print('  - start rest interface for healthcheck')
        r = RestInterface( port = rest_healthcheck_port )
        r.start()

    if(thread_watch_enabled):
        print('  - start thread watcher')
        w = ThreadWatch(service_count)
        w.start()

    for service in services:
        if(service == 'nginx'):
            nginx_plugins = services.get('nginx').get('plugins')
            if('nginx_logfiles' in nginx_plugins):
                _v = nginx_plugins.remove('nginx_logfiles')

                n = NginxLogfiles(name = service, graphyte = graphyte, instance_name = instance_name , kwargs = services[service] , log_level = log_level.upper() )
                n.start()

        f = Collector( name = service, graphyte = graphyte, instance_name = instance_name , kwargs = services[service] , log_level = log_level.upper() )
        f.start()

#

import sys
import re
import json
import base

class Tomcat(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def get_stats(self):
        """

        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "content-management-server",
            "master-live-server",
            "replication-live-server",
            "workflow-server",
            "content-feeder",
            "user-changes",
            "elastic-worker",
            "caefeeder-preview",
            "caefeeder-live",
            "studio",
            "sitemanager",
            "headless-server",
            "eds",
            "cae-preview",
            "cae-live-1",
            "cae-live-2",
            "cae-live-3",
            "cae-live-4",
            "cae-live-5",
            "cae-live-6",
            "cae-live-7",
            "cae-live-8",
            "cae-live-9",
            "cae-live"]

        valid, application = self._validate_application(valid_applications, self.jmx_application)

        if( valid == False ):
            self.logwarning( "%s is no valid application for Tomcat" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

        data = {
            "Manager": {
              "mbean": "Catalina:type=Manager,context=/%s,host=localhost" % (application),
              "attribute": [
                'processingTime',
                'duplicates',
                'maxActive',
                'maxActiveSessions',
                'sessionMaxAliveTime',
                'sessionExpireRate',
                'sessionAverageAliveTime',
                'sessionCounter',
                'sessionCreateRate',
                'rejectedSessions',
                'processExpiresFrequency',
                'activeSessions',
                'expiredSessions',
                ]
            },
        }

        if( self.spring_boot ):
            data["Manager"]["mbean"]    = "Tomcat:type=Manager,host=localhost,context=/"

        jmx_result         = self.read_jmx_data(data)

        # print(json.dumps( jmx_result, indent = 2 ))

        jmx_manager_data   = self.parse_bean_data(jmx_result, "Manager")
        manager_status     = jmx_manager_data.get("status")

        if(manager_status == 200):
            """
            """
            result.append({
                "key": ".".join( [ prefix, 'Manager', 'processing', 'time' ]),
                "value": jmx_manager_data.get("processingTime")
            })

            result.append({
                "key": ".".join( [ prefix, 'Manager', 'sessions', 'active' ]),
                "value": jmx_manager_data.get("activeSessions")
            })
            result.append({
                "key": ".".join( [ prefix, 'Manager', 'sessions', 'count' ]),
                "value": jmx_manager_data.get("sessionCounter")
            })
            result.append({
                "key": ".".join( [ prefix, 'Manager', 'sessions', 'expired' ]),
                "value": jmx_manager_data.get("expiredSessions")
            })
            result.append({
                "key": ".".join( [ prefix, 'Manager', 'sessions', 'alive_avg' ]),
                "value": jmx_manager_data.get("sessionAverageAliveTime")
            })
            result.append({
                "key": ".".join( [ prefix, 'Manager', 'sessions', 'rejected' ]),
                "value": jmx_manager_data.get("rejectedSessions")
            })
            result.append({
                "key": ".".join( [ prefix, 'Manager', 'sessions', 'duplicates' ]),
                "value": jmx_manager_data.get("duplicates")
            })
            result.append({
                "key": ".".join( [ prefix, 'Manager', 'sessions', 'max_alive' ]),
                "value": jmx_manager_data.get("sessionMaxAliveTime")
            })
            result.append({
                "key": ".".join( [ prefix, 'Manager', 'sessions', 'expire_rate' ]),
                "value": jmx_manager_data.get("sessionExpireRate")
            })
            result.append({
                "key": ".".join( [ prefix, 'Manager', 'sessions', 'create_rate' ]),
                "value": jmx_manager_data.get("sessionCreateRate")
            })
            result.append({
                "key": ".".join( [ prefix, 'Manager', 'sessions', 'expire_freq' ]),
                "value": jmx_manager_data.get("processExpiresFrequency")
            })

            maxActiveSessions = jmx_manager_data.get("maxActiveSessions")

            if( maxActiveSessions > -1):
                result.append({
                    "key": ".".join( [ prefix, 'Manager', 'sessions', 'max_active' ]),
                    "value": maxActiveSessions
                })


        return result

#

import sys
import re
import json
import base

class ConnectionPool(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def get_stats(self):
        """

        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "content-management-server",
            "master-live-server",
            "replication-live-server",
            "workflow-server"]

        valid, application = self._validate_application( valid_applications,  self.jmx_application )

        if( valid == False ):
            self.logwarning( "%s is no valid application for ConnectionPool" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

        data = {
            "Server": {
              "mbean": "com.coremedia:type=Store,bean=ConnectionPool,application=%s" % ( self.jmx_application ),
              "attribute": [
                'OpenConnections',
                'MaxConnections',
                'IdleConnections',
                'BusyConnections',
                'MinConnections']
            }
        }

        if( self.spring_boot ):
            data["Server"]["mbean"]           = "com.coremedia:type=Store,bean=ConnectionPool,application=coremedia"

        result   = self.read_jmx_data(data)
        #print(json.dumps( result, indent = 2 ))
        jmx_data = self.parse_bean_data(result, "Store")
        #print(json.dumps( jmx_data, indent = 2 ))
        status   = jmx_data.get("status", 404)
        result   = []

        if(status == 200):
            """
            """
            result.append({
              "key": ".".join( [ prefix, 'ConnectionPool', 'connections', 'open' ]),
              "value": jmx_data.get('OpenConnections', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'ConnectionPool', 'connections', 'max' ]),
              "value": jmx_data.get('MaxConnections', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'ConnectionPool', 'connections', 'idle' ]),
              "value": jmx_data.get('IdleConnections', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'ConnectionPool', 'connections', 'busy' ]),
              "value": jmx_data.get('BusyConnections', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'ConnectionPool', 'connections', 'min' ]),
              "value": jmx_data.get('MinConnections', 0)
            })

        return result

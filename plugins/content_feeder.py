#

import sys
import re
import json
import base

class ContentFeeder(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def get_stats(self):
        """

        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "content-feeder"]

        valid, application = self._validate_application( valid_applications,  self.jmx_application )

        if( valid == False ):
            self.logwarning( "%s is no valid application for CAEFeeder" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

#StateNumeric	2	-1	Return the state of the feeder with 0=stopped, 1=starting, 2=initializing, 3=running, 4=failed.
#State	initializing	-1	Return the state of the feeder.

        data = {
            "Feeder": {
              "mbean": 'com.coremedia:type=Feeder,application=%s' % (self.jmx_application),
              "attribute": [
                'State',
                'StateNumeric',
                'PendingEvents',
                'PersistedEvents',
                'TimestampOfLastPersistedEvent',
                'IndexBatches',
                'IndexContentDocuments',
                'IndexDocuments',
                'IndexOpenBatches',
                'Uptime']
            },
            "AdminBackgroundFeed": {
              "mbean": 'com.coremedia:type=AdminBackgroundFeed,application=%s' % (self.jmx_application),
              "attribute": [
                'NumberOfPendingContents',
                'State']
            },
            "LocationTaxonomyIdsBackgroundFeed": {
              "mbean": 'com.coremedia:type=LocationTaxonomyIdsBackgroundFeed,application=%s' % (self.jmx_application),
              "attribute": [
                'CurrentPendingContents',
                'State']
            },
            "PathBackgroundFeed": {
              "mbean": 'com.coremedia:type=PathBackgroundFeed,application=%s' % (self.jmx_application),
              "attribute": [
                'CurrentPendingContents',
                'State']
            },
            "SubjectTaxonomyIdsBackgroundFeed": {
              "mbean": 'com.coremedia:type=SubjectTaxonomyIdsBackgroundFeed,application=%s' % (self.jmx_application),
              "attribute": [
                'CurrentPendingContents',
                'State']
            },
            "UpdateGroupsBackgroundFeed": {
              "mbean": 'com.coremedia:type=UpdateGroupsBackgroundFeed,application=%s' % (self.jmx_application),
              "attribute": [
                'CurrentPendingContents',
                'State']
            },
            "SolrIndexer": {
              "mbean": 'com.coremedia:type=SolrIndexer,application=%s' % (self.jmx_application),
              "attribute": [
                'Collection',
                'Url']
            }


        }

        if( self.spring_boot ):
            data["Feeder"]["mbean"]                            = "com.coremedia:type=Feeder,application=coremedia"
            data["AdminBackgroundFeed"]["mbean"]               = "com.coremedia:type=AdminBackgroundFeed,application=coremedia"
            data["LocationTaxonomyIdsBackgroundFeed"]["mbean"] = "com.coremedia:type=LocationTaxonomyIdsBackgroundFeed,application=coremedia"
            data["PathBackgroundFeed"]["mbean"]                = "com.coremedia:type=PathBackgroundFeed,application=coremedia"
            data["SubjectTaxonomyIdsBackgroundFeed"]["mbean"]  = "com.coremedia:type=SubjectTaxonomyIdsBackgroundFeed,application=coremedia"
            data["UpdateGroupsBackgroundFeed"]["mbean"]        = "com.coremedia:type=UpdateGroupsBackgroundFeed,application=coremedia"
            data["SolrIndexer"]["mbean"]                       = "com.coremedia:type=SolrIndexer,application=coremedia"

        jmx_result   = self.read_jmx_data(data)
        #print(json.dumps( jmx_result, indent = 2 ))

        jmx_feeder_data         = self.parse_bean_data(jmx_result, "Feeder")
        jmx_admin_data          = self.parse_bean_data(jmx_result, "AdminBackgroundFeed")
        jmx_location_data       = self.parse_bean_data(jmx_result, "LocationTaxonomyIdsBackgroundFeed")
        jmx_path_data           = self.parse_bean_data(jmx_result, "PathBackgroundFeed")
        jmx_subject_data        = self.parse_bean_data(jmx_result, "SubjectTaxonomyIdsBackgroundFeed")
        jmx_update_groups_data  = self.parse_bean_data(jmx_result, "UpdateGroupsBackgroundFeed")
        jmx_solr_data           = self.parse_bean_data(jmx_result, "SolrIndexer")

        #print(json.dumps( jmx_feeder_data, indent = 2 ))
        feeder_status           = jmx_feeder_data.get("status", 404)
        admin_status            = jmx_admin_data.get("status", 404)
        location_status         = jmx_location_data.get("status", 404)
        path_status             = jmx_path_data.get("status", 404)
        subject_status          = jmx_subject_data.get("status", 404)
        update_groups_status    = jmx_update_groups_data.get("status", 404)
        solr_status             = jmx_solr_data.get("status", 404)

        if(feeder_status == 200):
            """
            """
            result.append({
                "key": ".".join( [ prefix, 'Feeder', 'uptime' ]),
                "value": jmx_feeder_data.get("Uptime", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Feeder', 'state' ]),
                "value": jmx_feeder_data.get("StateNumeric", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Feeder', 'pending_events' ]),
                "value": jmx_feeder_data.get("PendingEvents", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Feeder', 'index_documents' ]),
                "value": jmx_feeder_data.get("IndexDocuments", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Feeder', 'index_content_documents' ]),
                "value": jmx_feeder_data.get("IndexContentDocuments", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Feeder', 'current_pending_documents' ]),
                "value": jmx_feeder_data.get("CurrentPendingDocuments", 0)
            })

        if(admin_status == 200):
            """
            """
            result.append({
                "key": ".".join( [ prefix, 'Feeder', 'BackgroundFeed', 'admin', 'pending_contents' ]),
                "value": jmx_admin_data.get("NumberOfPendingContents", 0)
            })

        if(location_status == 200):
            """
            """
            result.append({
                "key": ".".join( [ prefix, 'Feeder', 'BackgroundFeed', 'location_taxonomy_ids', 'pending_contents' ]),
                "value": jmx_location_data.get("CurrentPendingContents", 0)
            })

        if(path_status == 200):
            """
            """
            result.append({
                "key": ".".join( [ prefix, 'Feeder', 'BackgroundFeed', 'path', 'pending_contents' ]),
                "value": jmx_path_data.get("CurrentPendingContents", 0)
            })

        if(subject_status == 200):
            """
            """
            result.append({
                "key": ".".join( [ prefix, 'Feeder', 'BackgroundFeed', 'subject_taxonomy_ids', 'pending_contents' ]),
                "value": jmx_subject_data.get("CurrentPendingContents", 0)
            })

        if(jmx_update_groups_data == 200):
            """
            """
            result.append({
                "key": ".".join( [ prefix, 'Feeder', 'BackgroundFeed', 'path', 'pending_contents' ]),
                "value": jmx_update_groups_data.get("CurrentPendingContents", 0)
            })


        return result

#

import sys
import re
import json
import base

from enum import Enum

class CacheClasses(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()

    def get_stats(self):
        """

        """
        result = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "studio",
            "eds",
            "cae-preview",
            "cae-live-1",
            "cae-live-2",
            "cae-live-3",
            "cae-live-4",
            "cae-live-5",
            "cae-live-6",
            "cae-live-7",
            "cae-live-8",
            "cae-live-9",
            "cae-live"]

        valid, application = self._validate_application( valid_applications,  self.jmx_application )

        if( valid == False ):
            self.logwarning( "%s is no valid application for CacheClasses" %(self.jmx_application) )
            return []

        data = {
            "CacheClasses": {
              "mbean": 'com.coremedia:type=Cache.Classes,CacheClass="*",application=%s' % (application),
              "attribute": [
                  "Capacity",
                  "Evicted",
                  "Level",
                  "MissRate",
                  "Removed"]
            }
        }

        if( self.spring_boot ):
            if( application == 'cae-live' or application == 'cae-preview' or application == 'eds' ):
                data["CacheClasses"]["mbean"] = 'com.coremedia:type=Cache.Classes,CacheClass="*",application=blueprint'
            else:
                data["CacheClasses"]["mbean"] = 'com.coremedia:type=Cache.Classes,CacheClass="*",application=coremedia'

        # print(json.dumps( data, indent = 2 ))
        jmx_result  = self.read_jmx_data(data)
        # print(json.dumps( jmx_result, indent = 2 ))
        jmx_data    = self.parse_bean_data(jmx_result, "Cache.Classes")
        # print(json.dumps( jmx_data, indent = 2 ))
        status      = jmx_data.get("status")
        prefix      = self.normalize_service(self.jmx_application)

        if(status == 200):
            """
            """
            for d,v  in jmx_data.items():
                if( d.startswith("com.coremedia:CacheClass=")):

                    # jump over
                    #    com.coremedia:CacheClass="java.lang.Object",application=cae-preview,type=Cache.Classes
                    if "java.lang.Object" in d:
                        continue

                    cache_class_type = ''
                    pattern = (r'^com.coremedia:CacheClass="(?P<cache_class_type>.+?)",application=(?P<application>.*?),type=(?P<type>.*$)')

                    regex = re.compile(pattern)
                    m = regex.match(d)
                    d = m.groupdict() if m is not None else None
                    # {'cache_class_type': 'DIGEST', 'application': 'cae-preview', 'type': 'Cache.Classes'}
                    cache_class_type = d.get('cache_class_type')
                    # split the string and get the last entry and capitalize the result
                    cache_class_type = cache_class_type.rsplit('.', 1)[-1]
                    cache_class_type = cache_class_type[0].upper() + cache_class_type[1:]

                    for i,j in v.items():

                        result.append({
                            "key": ".".join( [ prefix, 'CacheClasses', cache_class_type, i ]),
                            "value": j
                        })

        return result

#

from __future__ import print_function

import sys
import re
import json
import base

class Java(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )


    def get_stats(self):
        """

        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "content-management-server",
            "master-live-server",
            "replication-live-server",
            "workflow-server",
            "content-feeder",
            "user-changes",
            "elastic-worker",
            "caefeeder-preview",
            "caefeeder-live",
            "studio",
            "solr",
            "solr-master",
            "solr-slave",
            "sitemanager",
            "headless-server",
            "eds",
            "cae-preview",
            "cae-live-1",
            "cae-live-2",
            "cae-live-3",
            "cae-live-4",
            "cae-live-5",
            "cae-live-6",
            "cae-live-7",
            "cae-live-8",
            "cae-live-9",
            "cae-live"]

        valid, application = self._validate_application(valid_applications, self.jmx_application)

        if( valid == False ):
            self.logwarning( "%s is no valid application for Java" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

        data = {
            "Runtime": {
              "mbean": "java.lang:type=Runtime",
              "attribute": [
                'Uptime',
                'StartTime']
            },
            "Memory": {
              "mbean": "java.lang:type=Memory",
              "attribute": [
                'HeapMemoryUsage',
                'NonHeapMemoryUsage']
            },
            "Threading": {
              "mbean": "java.lang:type=Threading",
              "attribute": [
                'PeakThreadCount',
                'ThreadCount']
            },
            "ClassLoading": {
              "mbean": "java.lang:type=ClassLoading",
              "attribute": [
                'LoadedClassCount',
                'TotalLoadedClassCount',
                'UnloadedClassCount']
            },
            "OperatingSystem": {
              "mbean": "java.lang:type=OperatingSystem",
              "attribute": [
                'OpenFileDescriptorCount',     # The number of open file descriptors.
                'MaxFileDescriptorCount',      # The maximum number of file descriptors.
                'SystemCpuLoad',               # The CPU load of the machine running the JVM in percent of max usage. The machine is running on full load when it reaches 100. If the recent CPU load is not available, the value will be negative.
                'ProcessCpuLoad',              # The amount of CPU load, as a value between 0.0 and 1.0, used by the JVM. When this value is 0.0, the JVM does not use the CPU. If the recent CPU load is not available, the value will be negative.
                'CommittedVirtualMemorySize',  # The amount of virtual memory that i1s guaranteed to be available to the running process, or -1 if this operation is not supported.
                # 'PhysicalMemoryUsagePercent',  # The amount, measured in percentage, of physical memory that is used by the JVM.
                'TotalPhysicalMemorySize',     # The total amount of physical memory.
                # 'UsedPhysicalMemorySize',      # The amount of memory that the system is using.
                'FreePhysicalMemorySize',      # The amount of free physical memory.
                'TotalSwapSpaceSize',          # The total amount of swap space memory.
                # 'UsedSwapSpaceSize',           # The amount of memory that the system is swapping.
                'ProcessCpuTime',              # Returns the CPU time used by the process on which the JVM is running in nanoseconds. The returned value is of nanoseconds precision but not necessarily nanoseconds accuracy.
                'FreeSwapSpaceSize'            # The amount of free swap memory space.
              ]
            }
        }

        jmx_result         = self.read_jmx_data(data)

        # print(json.dumps( jmx_result, indent = 2 ))

        jmx_runtime_data   = self.parse_bean_data(jmx_result, "Runtime")
        jmx_memory_data    = self.parse_bean_data(jmx_result, "Memory")
        jmx_threading_data = self.parse_bean_data(jmx_result, "Threading")
        jmx_classload_data = self.parse_bean_data(jmx_result, "ClassLoading")
        jmx_operating_data = self.parse_bean_data(jmx_result, "OperatingSystem")

        runtime_status     = jmx_runtime_data.get("status")
        memory_status      = jmx_memory_data.get("status")
        threading_status   = jmx_threading_data.get("status")
        classload_status   = jmx_classload_data.get("status")
        operating_status   = jmx_operating_data.get("status")

        if(runtime_status == 200):
            result.append({
                "key": ".".join( [ prefix, 'java', 'Runtime', 'uptime' ]),
                "value": jmx_runtime_data.get("Uptime")
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'Runtime', 'starttime' ]),
                "value": jmx_runtime_data.get("StartTime")
            })

        if(memory_status == 200):
            """
            """
            # print(json.dumps( jmx_memory_data, indent = 2 ))

            memory_types = ['HeapMemoryUsage', 'NonHeapMemoryUsage']

            for mem_type in memory_types:

                type = 'heap_memory'
                if(mem_type == 'NonHeapMemoryUsage'):
                    type = 'perm_memory'

                memory = jmx_memory_data.get(mem_type)

                init      = memory.get('init')
                max       = memory.get('max')
                used      = memory.get('used')
                committed = memory.get('committed')

                if( max > 1 ):
                    percent   = round( ( 100 * used / max ), 3)
                else:
                    percent   = round( ( 100 * used / committed ), 3)

                result.append({
                    "key": ".".join( [ prefix, 'java', 'Memory', type, 'init' ]),
                    "value": init
                })
                result.append({
                    "key": ".".join( [ prefix, 'java', 'Memory', type, 'max' ]),
                    "value": max
                })
                result.append({
                    "key": ".".join( [ prefix, 'java', 'Memory', type, 'used' ]),
                    "value": used
                })
                result.append({
                    "key": ".".join( [ prefix, 'java', 'Memory', type, 'committed' ]),
                    "value": committed
                })
                result.append({
                    "key": ".".join( [ prefix, 'java', 'Memory', type, 'used_percent' ]),
                    "value": percent
                })

        if(threading_status == 200):
            result.append({
                "key": ".".join( [ prefix, 'java', 'Threading', 'count' ]),
                "value": jmx_threading_data.get("ThreadCount")
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'Threading', 'peak' ]),
                "value": jmx_threading_data.get("PeakThreadCount")
            })

        if(classload_status == 200):
            result.append({
                "key": ".".join( [ prefix, 'java', 'ClassLoading', 'loaded' ]),
                "value": jmx_classload_data.get("LoadedClassCount")
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'ClassLoading', 'total' ]),
                "value": jmx_classload_data.get("TotalLoadedClassCount")
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'ClassLoading', 'unloaded' ]),
                "value": jmx_classload_data.get("UnloadedClassCount")
            })

        if(operating_status == 200):
            """
            """
            # print(json.dumps( jmx_operating_data, indent = 2 ))

            phy_memory_size     = int(jmx_operating_data.get("TotalPhysicalMemorySize", 0))
            phy_memory_free     = int(jmx_operating_data.get("FreePhysicalMemorySize", 0))
            phy_memory_used     = phy_memory_size - phy_memory_free
            phy_memory_percent  = ( 100 * phy_memory_used / phy_memory_size )

            swap_memory_size    = jmx_operating_data.get("TotalSwapSpaceSize", 0)
            swap_memory_free    = jmx_operating_data.get("FreeSwapSpaceSize", 0)

            if(swap_memory_size > 0):
                swap_memory_used    = swap_memory_size - swap_memory_free
                swap_memory_percent = ( 100 * swap_memory_used / swap_memory_size )
            else:
                swap_memory_used    = 0
                swap_memory_percent = 0

            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'file_descriptor', 'open' ]),
                "value": jmx_operating_data.get("OpenFileDescriptorCount", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'file_descriptor', 'max' ]),
                "value": jmx_operating_data.get("MaxFileDescriptorCount", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'process_cpu_time' ]),
                "value": jmx_operating_data.get("ProcessCpuTime", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'system_cpu_load' ]),
                "value": round(jmx_operating_data.get("SystemCpuLoad", 0.0), 3)
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'process_cpu_load' ]),
                "value": round(jmx_operating_data.get("ProcessCpuLoad", 0.0), 3)
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'commited_virtual_memory_size' ]),
                "value": jmx_operating_data.get("CommittedVirtualMemorySize", 0)
            })

            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'physical_memory', 'size' ]),
                "value": jmx_operating_data.get("TotalPhysicalMemorySize", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'physical_memory', 'free' ]),
                "value": jmx_operating_data.get("FreePhysicalMemorySize", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'physical_memory', 'used' ]),
                "value": phy_memory_used
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'physical_memory', 'percent_usage' ]),
                "value": round(phy_memory_percent, 3)
            })

            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'swap', 'size' ]),
                "value": swap_memory_size
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'swap', 'free' ]),
                "value": swap_memory_free
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'swap', 'used' ]),
                "value": swap_memory_used
            })
            result.append({
                "key": ".".join( [ prefix, 'java', 'OperatingSystem', 'swap', 'percent_usage' ]),
                "value": round(swap_memory_percent, 3)
            })


        return result

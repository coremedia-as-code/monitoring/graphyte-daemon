#

from __future__ import print_function

import sys
import re
import json
import base
import time
import logging

try:
  import MySQLdb
except ImportError:
  print( sys.exc_info() )
  sys.exit(1)

# import pprint
# pp = pprint.PrettyPrinter(indent=2)

MYSQL_STATUS_VARS = {
	'Aborted_clients': 'counter',
	'Aborted_connects': 'counter',
	'Binlog_cache_disk_use': 'counter',
	'Binlog_cache_use': 'counter',
	'Bytes_received': 'counter',
	'Bytes_sent': 'counter',
	'Connections': 'counter',
	'Created_tmp_disk_tables': 'counter',
	'Created_tmp_files': 'counter',
	'Created_tmp_tables': 'counter',
	'Innodb_buffer_pool_pages_data': 'gauge',
	'Innodb_buffer_pool_pages_dirty': 'gauge',
	'Innodb_buffer_pool_pages_free': 'gauge',
	'Innodb_buffer_pool_pages_total': 'gauge',
	'Innodb_buffer_pool_read_requests': 'counter',
	'Innodb_buffer_pool_reads': 'counter',
	'Innodb_checkpoint_age': 'gauge',
	'Innodb_checkpoint_max_age': 'gauge',
	'Innodb_data_fsyncs': 'counter',
	'Innodb_data_pending_fsyncs': 'gauge',
	'Innodb_data_pending_reads': 'gauge',
	'Innodb_data_pending_writes': 'gauge',
	'Innodb_data_read': 'counter',
	'Innodb_data_reads': 'counter',
	'Innodb_data_writes': 'counter',
	'Innodb_data_written': 'counter',
	'Innodb_deadlocks': 'counter',
	'Innodb_history_list_length': 'gauge',
	'Innodb_ibuf_free_list': 'gauge',
	'Innodb_ibuf_merged_delete_marks': 'counter',
	'Innodb_ibuf_merged_deletes': 'counter',
	'Innodb_ibuf_merged_inserts': 'counter',
	'Innodb_ibuf_merges': 'counter',
	'Innodb_ibuf_segment_size': 'gauge',
	'Innodb_ibuf_size': 'gauge',
	'Innodb_lsn_current': 'counter',
	'Innodb_lsn_flushed': 'counter',
	'Innodb_max_trx_id': 'counter',
	'Innodb_mem_adaptive_hash': 'gauge',
	'Innodb_mem_dictionary': 'gauge',
	'Innodb_mem_total': 'gauge',
	'Innodb_mutex_os_waits': 'counter',
	'Innodb_mutex_spin_rounds': 'counter',
	'Innodb_mutex_spin_waits': 'counter',
	'Innodb_os_log_pending_fsyncs': 'gauge',
	'Innodb_pages_created': 'counter',
	'Innodb_pages_read': 'counter',
	'Innodb_pages_written': 'counter',
	'Innodb_row_lock_time': 'counter',
	'Innodb_row_lock_time_avg': 'gauge',
	'Innodb_row_lock_time_max': 'gauge',
	'Innodb_row_lock_waits': 'counter',
	'Innodb_rows_deleted': 'counter',
	'Innodb_rows_inserted': 'counter',
	'Innodb_rows_read': 'counter',
	'Innodb_rows_updated': 'counter',
	'Innodb_s_lock_os_waits': 'counter',
	'Innodb_s_lock_spin_rounds': 'counter',
	'Innodb_s_lock_spin_waits': 'counter',
	'Innodb_uncheckpointed_bytes': 'gauge',
	'Innodb_unflushed_log': 'gauge',
	'Innodb_unpurged_txns': 'gauge',
	'Innodb_x_lock_os_waits': 'counter',
	'Innodb_x_lock_spin_rounds': 'counter',
	'Innodb_x_lock_spin_waits': 'counter',
	'Key_blocks_not_flushed': 'gauge',
	'Key_blocks_unused': 'gauge',
	'Key_blocks_used': 'gauge',
	'Key_read_requests': 'counter',
	'Key_reads': 'counter',
	'Key_write_requests': 'counter',
	'Key_writes': 'counter',
	'Max_used_connections': 'gauge',
	'Open_files': 'gauge',
	'Open_table_definitions': 'gauge',
	'Open_tables': 'gauge',
	'Opened_files': 'counter',
	'Opened_table_definitions': 'counter',
	'Opened_tables': 'counter',
	'Qcache_free_blocks': 'gauge',
	'Qcache_free_memory': 'gauge',
	'Qcache_hits': 'counter',
	'Qcache_inserts': 'counter',
	'Qcache_lowmem_prunes': 'counter',
	'Qcache_not_cached': 'counter',
	'Qcache_queries_in_cache': 'counter',
	'Qcache_total_blocks': 'counter',
	'Questions': 'counter',
	'Select_full_join': 'counter',
	'Select_full_range_join': 'counter',
	'Select_range': 'counter',
	'Select_range_check': 'counter',
	'Select_scan': 'counter',
	'Slave_open_temp_tables': 'gauge',
	'Slave_retried_transactions': 'counter',
	'Slow_launch_threads': 'counter',
	'Slow_queries': 'counter',
	'Sort_merge_passes': 'counter',
	'Sort_range': 'counter',
	'Sort_rows': 'counter',
	'Sort_scan': 'counter',
	'Table_locks_immediate': 'counter',
	'Table_locks_waited': 'counter',
	'Table_open_cache_hits': 'counter',
	'Table_open_cache_misses': 'counter',
	'Table_open_cache_overflows': 'counter',
	'Threadpool_idle_threads': 'gauge',
	'Threadpool_threads': 'gauge',
	'Threads_cached': 'gauge',
	'Threads_connected': 'gauge',
	'Threads_created': 'counter',
	'Threads_running': 'gauge',
	'Uptime': 'gauge',
	'wsrep_apply_oooe': 'gauge',
	'wsrep_apply_oool': 'gauge',
	'wsrep_apply_window': 'gauge',
	'wsrep_causal_reads': 'gauge',
	'wsrep_cert_deps_distance': 'gauge',
	'wsrep_cert_index_size': 'gauge',
	'wsrep_cert_interval': 'gauge',
	'wsrep_cluster_size': 'gauge',
	'wsrep_commit_oooe': 'gauge',
	'wsrep_commit_oool': 'gauge',
	'wsrep_commit_window': 'gauge',
	'wsrep_flow_control_paused': 'gauge',
	'wsrep_flow_control_paused_ns': 'counter',
	'wsrep_flow_control_recv': 'counter',
	'wsrep_flow_control_sent': 'counter',
	'wsrep_local_bf_aborts': 'counter',
	'wsrep_local_cert_failures': 'counter',
	'wsrep_local_commits': 'counter',
	'wsrep_local_recv_queue': 'gauge',
	'wsrep_local_recv_queue_avg': 'gauge',
	'wsrep_local_recv_queue_max': 'gauge',
	'wsrep_local_recv_queue_min': 'gauge',
	'wsrep_local_replays': 'gauge',
	'wsrep_local_send_queue': 'gauge',
	'wsrep_local_send_queue_avg': 'gauge',
	'wsrep_local_send_queue_max': 'gauge',
	'wsrep_local_send_queue_min': 'gauge',
	'wsrep_received': 'counter',
	'wsrep_received_bytes': 'counter',
	'wsrep_repl_data_bytes': 'counter',
	'wsrep_repl_keys': 'counter',
	'wsrep_repl_keys_bytes': 'counter',
	'wsrep_repl_other_bytes': 'counter',
	'wsrep_replicated': 'counter',
	'wsrep_replicated_bytes': 'counter',
}



MYSQL_INNODB_STATUS_VARS = {
    'active_transactions': 'gauge',
    'current_transactions': 'gauge',
    'file_reads': 'counter',
    'file_system_memory': 'gauge',
    'file_writes': 'counter',
    'innodb_lock_structs': 'gauge',
    'innodb_lock_wait_secs': 'gauge',
    'innodb_locked_tables': 'gauge',
    'innodb_sem_wait_time_ms': 'gauge',
    'innodb_sem_waits': 'gauge',
    'innodb_tables_in_use': 'gauge',
    'lock_system_memory': 'gauge',
    'locked_transactions': 'gauge',
    'log_writes': 'counter',
    'page_hash_memory': 'gauge',
    'pending_aio_log_ios': 'gauge',
    'pending_buf_pool_flushes': 'gauge',
    'pending_chkp_writes': 'gauge',
    'pending_ibuf_aio_reads': 'gauge',
    'pending_log_writes':'gauge',
    'queries_inside': 'gauge',
    'queries_queued': 'gauge',
    'read_views': 'gauge',
}

MYSQL_INNODB_STATUS_MATCHES = {
    # 0 read views open inside InnoDB
    'read views open inside InnoDB': {
        'read_views': 0,
    },
    # 5635328 OS file reads, 27018072 OS file writes, 20170883 OS fsyncs
    ' OS file reads, ': {
        'file_reads': 0,
        'file_writes': 4,
    },
    # ibuf aio reads: 0, log i/o's: 0, sync i/o's: 0
    'ibuf aio reads': {
        'pending_ibuf_aio_reads': 3,
        'pending_aio_log_ios': 6,
        'pending_aio_sync_ios': 9,
    },
    # Pending flushes (fsync) log: 0; buffer pool: 0
    'Pending flushes (fsync)': {
        'pending_buf_pool_flushes': 7,
    },
    # 16086708 log i/o's done, 106.07 log i/o's/second
    " log i/o's done, ": {
        'log_writes': 0,
    },
    # 0 pending log writes, 0 pending chkp writes
    ' pending log writes, ': {
        'pending_log_writes': 0,
        'pending_chkp_writes': 4,
    },
    # Page hash           2302856 (buffer pool 0 only)
    'Page hash    ': {
        'page_hash_memory': 2,
    },
    # File system         657820264     (812272 + 657007992)
    'File system    ': {
        'file_system_memory': 2,
    },
    # Lock system         143820296     (143819576 + 720)
    'Lock system    ': {
        'lock_system_memory': 2,
    },
    # 0 queries inside InnoDB, 0 queries in queue
    'queries inside InnoDB, ': {
        'queries_inside': 0,
        'queries_queued': 4,
    },
    # --Thread 139954487744256 has waited at dict0dict.cc line 472 for 0.0000 seconds the semaphore:
    'seconds the semaphore': {
        'innodb_sem_waits': lambda row, stats: stats['innodb_sem_waits'] + 1,
        'innodb_sem_wait_time_ms': lambda row, stats: int(float(row[9]) * 1000),
    },
    # mysql tables in use 1, locked 1
    'mysql tables in use': {
        'innodb_tables_in_use': lambda row, stats: stats['innodb_tables_in_use'] + int(row[4]),
        'innodb_locked_tables': lambda row, stats: stats['innodb_locked_tables'] + int(row[6]),
    },
    "------- TRX HAS BEEN": {
        "innodb_lock_wait_secs": lambda row, stats: stats['innodb_lock_wait_secs'] + int(row[5]),
    },
}

class Mysql(base.Base):
    """

    """

    mysql_process_states = {
        'closing_tables': 0,
        'copying_to_tmp_table': 0,
        'end': 0,
        'freeing_items': 0,
        'init': 0,
        'locked': 0,
        'login': 0,
        'none': 0,
        'other': 0,
        'preparing': 0,
        'reading_from_net': 0,
        'sending_data': 0,
        'sorting_result': 0,
        'statistics': 0,
        'updating': 0,
        'writing_to_net': 0,
        'creating_table': 0,
        'opening_tables': 0,
    }



    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.mysql_host     = kwargs.get("mysql_host", "127.0.0.1" )
        self.mysql_username = kwargs.get("mysql_username", 'monitoring' )
        self.mysql_password = kwargs.get("mysql_password", 'monitoring' )
        self.mysql_database = 'monitoring'
        self.mysql_port     = 3306

    def _connection_to(self):
        return MySQLdb.connect(self.mysql_host, self.mysql_username, self.mysql_password, self.mysql_database)


    def __mysql_query(self, query):
        """
        """
        #print(query)
        cursor = self._connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(query)

        return cursor


    def __fetch_mysql_status(self):
        """
        """
        status = {}

        data   = self.__mysql_query('SHOW STATUS')

        values = data.fetchall()

        for row in values:
          status[row['Variable_name']] = row['Value']

        # calculate the number of unpurged txns from existing variables
        if('Innodb_max_trx_id' in status):
            status['Innodb_unpurged_txns'] = int(status['Innodb_max_trx_id']) - int(status['Innodb_purge_trx_id'])

        if('Innodb_lsn_last_checkpoint' in status):
            status['Innodb_uncheckpointed_bytes'] = int(status['Innodb_lsn_current'])- int(status['Innodb_lsn_last_checkpoint'])

        if('Innodb_lsn_flushed' in status):
            status['Innodb_unflushed_log'] = int(status['Innodb_lsn_current']) - int(status['Innodb_lsn_flushed'])

        return status


    def __fetch_mysql_process_states(self):
        """
        """

        result = self.__mysql_query('SHOW PROCESSLIST')

        states = self.mysql_process_states.copy()

        for row in result.fetchall():
            state = row.get('State')

            if state not in states:
                self.logdebug("'%s' are not in states" % (state))
                continue
              #state = 'other'

            if state == '' or state == None: state = 'none'
            state = re.sub(r'^(Table lock|Waiting for .*lock)$', "Locked", state)
            state = state.lower().replace(" ", "_")
            # if state not in states: state = 'other'

            states[state] += 1

        return states


    def __fetch_innodb_stats(self):
        """
        """
        global MYSQL_INNODB_STATUS_MATCHES, MYSQL_INNODB_STATUS_VARS

        try:
            result = self.__mysql_query('SHOW ENGINE INNODB STATUS')
        except Exception as error:
            self.logerror(error)
            return {}

        row    = result.fetchone()
        status = row['Status']
        stats  = dict.fromkeys(MYSQL_INNODB_STATUS_VARS.keys(), 0)

        for line in status.split("\n"):
            line = line.strip()
            row  = re.split(r' +', re.sub(r'[,;] ', ' ', line))
            if line == '': continue

            # ---TRANSACTION 124324402462, not started
            # ---TRANSACTION 124324402468, ACTIVE 0 sec committing
            if line.find("---TRANSACTION") != -1:
                stats['current_transactions'] += 1
                if line.find("ACTIVE") != -1:
                    stats['active_transactions'] += 1
            # LOCK WAIT 228 lock struct(s), heap size 46632, 65 row lock(s), undo log entries 1
            # 205 lock struct(s), heap size 30248, 37 row lock(s), undo log entries 1
            elif line.find("lock struct(s)") != -1:
                if line.find("LOCK WAIT") != -1:
                    stats['innodb_lock_structs'] += int(row[2])
                    stats['locked_transactions'] += 1
                else:
                    stats['innodb_lock_structs'] += int(row[0])
            else:
                for match in MYSQL_INNODB_STATUS_MATCHES:
                    if line.find(match) == -1: continue
                    for key in MYSQL_INNODB_STATUS_MATCHES[match]:
                        value = MYSQL_INNODB_STATUS_MATCHES[match][key]
                        if type(value) is int:
                            if value < len(row) and row[value].isdigit():
                                stats[key] = int(row[value])
                        else:
                            stats[key] = value(row, stats)
                    break

        return stats



    def get_stats(self):
        """

        """
        result         = []
        self.prefix    = self.normalize_service('mysql')

        try:
            self._connection = self._connection_to()
        except ConnectionError as e:
            self.logerror(e)

        mysql_status  = self.__fetch_mysql_status()
        mysql_states  = self.__fetch_mysql_process_states()
        #innodb_status = self.__fetch_innodb_stats()

        self._connection.close()

        #  pp.pprint( mysql_status )

        for key in mysql_states:
            result.append({
                "key": ".".join( [ self.prefix, 'process_states', key.lower() ]),
                "value": int(mysql_states.get(key))
            })

        # innodb       = { k: v for k, v in mysql_status.items() if k.startswith('Innodb_') }
        innodb_row   = { k: v for k, v in mysql_status.items() if k.startswith('Innodb_rows_') }
        innodb_pages = { k: v for k, v in mysql_status.items() if k.startswith('Innodb_pages_') }
        threads      = { k: v for k, v in mysql_status.items() if k.startswith('Threads') }
        qcache       = { k: v for k, v in mysql_status.items() if k.startswith('Qcache') }
        handler      = { k: v for k, v in mysql_status.items() if k.startswith('Handler') }
        # commands     = { k: v for k, v in mysql_status.items() if k.startswith('Com') }
        # open         = { k: v for k, v in mysql_status.items() if k.startswith('Open') }
        # select       = { k: v for k, v in mysql_status.items() if k.startswith('Select') }

        for k,v in threads.items():
            """
            """
            result.append({
                "key": ".".join( [ self.prefix ] + k.lower().split('_') ),
                "value": int(v)
            })

        for k,v in qcache.items():
            """
            """
            if k in ['Qcache_free_blocks', 'Qcache_free_memory', 'Qcache_hits', 'Qcache_inserts']:
               result.append({
                   "key": ".".join( [ self.prefix ] + k.lower().split('_') ),
                   "value": int(v)
               })
            else:
               result.append({
                   "key": ".".join( [ self.prefix, 'qcache', k.lower().replace('qcache_', '') ] ),
                   "value": int(v)
               })

        for k,v in handler.items():
            """
            """
            k = k.lower()

            if not k in ['handler_read_rnd_next', 'handler_read_rnd_deleted', 'handler_savepoint_rollback']:
                result.append({
                    "key": ".".join( [ self.prefix ] + k.split('_') ),
                    "value": int(v)
                })
            else:
              k = k.replace('handler_','')

              if(k.startswith('read_')):
                result.append({
                   "key": ".".join( [ self.prefix, 'handler', 'read', k.replace('read_', '') ] ),
                   "value": int(v)
                })
              else:
                result.append({
                   "key": ".".join( [ self.prefix, 'handler', k ] ),
                   "value": int(v)
                })

        for k,v in innodb_row.items():
            """
            """
            k = k.lower()
            result.append({
                "key": ".".join( [ self.prefix ] + k.lower().split('_') ),
                "value": int(v)
            })

        for k,v in innodb_pages.items():
            """
            """
            k = k.lower()
            result.append({
                "key": ".".join( [ self.prefix ] + k.lower().split('_') ),
                "value": int(v)
            })


        result.append({
            "key": ".".join( [ self.prefix, 'uptime' ]),
            "value": int(mysql_status.get('Uptime'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'open', 'files' ]),
            "value": int(mysql_status.get('Open_files'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'open', 'tables' ]),
            "value": int(mysql_status.get('Open_tables'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'open', 'table_definitions' ]),
            "value": int(mysql_status.get('Open_table_definitions'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'open', 'streams' ]),
            "value": int(mysql_status.get('Open_streams'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'memory', 'used' ]),
            "value": int(mysql_status.get('Memory_used'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'memory', 'used_initial' ]),
            "value": int(mysql_status.get('Memory_used_initial'))
        })

        result.append({
            "key": ".".join( [ self.prefix, 'network', 'bytes', 'tx' ]),
            "value": int(mysql_status.get('Bytes_received'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'network', 'bytes', 'rx' ]),
            "value": int(mysql_status.get('Bytes_sent'))
        })

        result.append({
            "key": ".".join( [ self.prefix, 'connections' ]),
            "value": int(mysql_status.get('Connections'))
        })

        result.append({
            "key": ".".join( [ self.prefix, 'aborted', 'clients' ]),
            "value": int(mysql_status.get('Aborted_clients'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'aborted', 'connects' ]),
            "value": int(mysql_status.get('Aborted_connects'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'access', 'denied' ]),
            "value": int(mysql_status.get('Access_denied_errors'))
        })

        result.append({
            "key": ".".join( [ self.prefix, 'connection', 'errors', 'internal' ]),
            "value": int(mysql_status.get('Connection_errors_internal'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'connection', 'errors', 'max_connections' ]),
            "value": int(mysql_status.get('Connection_errors_max_connections'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'connection', 'errors', 'accept' ]),
            "value": int(mysql_status.get('Connection_errors_accept'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'connection', 'errors', 'select' ]),
            "value": int(mysql_status.get('Connection_errors_select'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'connection', 'errors', 'peer_address' ]),
            "value": int(mysql_status.get('Connection_errors_peer_address'))
        })

        result.append({
            "key": ".".join( [ self.prefix, 'created', 'tmp', 'disk_tables' ]),
            "value": int(mysql_status.get('Created_tmp_disk_tables'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'created', 'tmp', 'files' ]),
            "value": int(mysql_status.get('Created_tmp_files'))
        })
        result.append({
            "key": ".".join( [ self.prefix, 'created', 'tmp', 'tables' ]),
            "value": int(mysql_status.get('Created_tmp_tables'))
        })


        return result

# https://github.com/chrisboulton/collectd-python-mysql/blob/master/mysql.py
# https://github.com/PyMySQL/mysqlclient-python

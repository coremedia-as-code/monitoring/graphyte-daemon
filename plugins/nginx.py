#

from __future__ import print_function

import sys
import re
import json
import base
import logging

from requests import get
from requests.exceptions import ConnectionError

class Nginx(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.nginx_status_url = kwargs.get("nginx_status_url", "http://127.0.0.1:80/nginx_status" )


    def __request_data(self, url):
        """
          executes get request to retrieve data from given url
        """

        logging.captureWarnings(True)

        try:
            response = get(url)

            # check response content type to determine which actuator api to be used
            if response.ok:
                return 200, response, None
            else:
                return response.status_code, Exception(response.status_code, url), None

        except ConnectionRefusedError as e:
            logging.error('Failed to establish a new connection: {} '.format(url) )
            return 404, None, msg

        except ConnectionError as e:
            msg = 'error fetching data from {}' . format(url)
            #logging.error('error fetching data from {}'.format(url))
            #logging.debug('error fetching data from {}'.format(url))
            return 404, None, msg

        finally:
            logging.captureWarnings(False)


    def get_stats(self):
        """

        """
        result        = []
        prefix        = self.normalize_service('nginx')

        status, response, message =  self.__request_data(self.nginx_status_url)

        if(status == 200):
            """
            """
            result = []

            content_type = response.headers['Content-Type']
            content_body = response.text

            """
            example output:
            Active connections: 2
            server accepts handled requests
             50 50 73
            Reading: 0 Writing: 1 Waiting: 1

            explain:

            * Active connections
              The current number of active client connections including Waiting connections.
            * accepts
              The total number of accepted client connections.
            * handled
              The total number of handled connections. Generally, the parameter value is the same as accepts unless some resource limits have been reached (for example, the worker_connections limit).
            * requests
              The total number of client requests.
            * Reading
              The current number of connections where nginx is reading the request header.
            * Writing
              The current number of connections where nginx is writing the response back to the client.
            * Waiting
              The current number of idle client connections waiting for a request.
            """
            conn = content_body.split("\n")

            # ['Active connections: 2 ', 'server accepts handled requests', ' 50 50 73 ', 'Reading: 0 Writing: 1 Waiting: 1 ', '']
            connections = conn[0]
            server      = conn[2]
            requests    = conn[3]

            connections = connections.split()    # ['Active', 'connections:', '2']
            server      = server.split()         # ['101', '101', '146']
            requests    = requests.split()       # ['Reading:', '0', 'Writing:', '1', 'Waiting:', '1']

            req_accepted = int(server[0])
            req_handled  = int(server[1])
            req_requests = int(server[2])
            conn_active  = int(connections[2])
            conn_reading = int(requests[1])
            conn_writing = int(requests[3])
            conn_waiting = int(requests[5])

            result.append({
                "key": ".".join( [ prefix, 'connections', 'active' ]),
                "value": conn_active
            })
            result.append({
                "key": ".".join( [ prefix, 'connections', 'reading' ]),
                "value": conn_reading
            })
            result.append({
                "key": ".".join( [ prefix, 'connections', 'writing' ]),
                "value": conn_writing
            })
            result.append({
                "key": ".".join( [ prefix, 'connections', 'waiting' ]),
                "value": conn_waiting
            })

            result.append({
                "key": ".".join( [ prefix, 'server', 'requests_accepted' ]),
                "value": req_accepted
            })
            result.append({
                "key": ".".join( [ prefix, 'server', 'requests_handled' ]),
                "value": req_handled
            })
            result.append({
                "key": ".".join( [ prefix, 'server', 'requests' ]),
                "value": req_requests
            })

        return result

# https://github.com/lebinh/ngxtop
# https://www.scalyr.com/blog/guide-to-nginx-metrics
# https://medium.com/faun/total-nginx-monitoring-with-application-performance-and-a-bit-more-using-8fc6d731051b

#!/usr/bin/env python
# coding=utf-8
#
#  Copyright (c) 2019-2020 Bodo Schulz
#
#
# collectd-coremedia-license
#
# https://gitlab.cern.ch/ikadochn/collectd-eos/blob/master/src/collectd_eos/__init__.py
# https://gist.github.com/dizz/1250968

# from collections import defaultdict

import re
import traceback
import socket
import json
from datetime import datetime
from pyjolokia import Jolokia, JolokiaError
from errno import ECONNREFUSED

# https://programtalk.com/vs2/?source=python/11188/collectd-ceph/plugins/base.py
#
class Base(object):

    def __init__(self):
        self.verbose = True
        self.debug = False
        self.prefix = ''
        self.interval = 60.0
        self.jolokia_url = "http://localhost:8080/jolokia/"
        self.application = None
        self.jmx_port = None
        self.jmx_application = None

        #print("Base::__init__")


    def get_stats(self):
        print('ERROR: Not implemented, should be subclassed')


    def parse_url(self, url):
        """
          extract all data from an url
        """
        #print(mls_ior)

        pattern = (r'^'
                   r'((?P<schema>.+?)://)?'
                   r'((?P<user>.+?)(:(?P<password>.*?))?@)?'
                   r'(?P<host>.*?)'
                   r'(:(?P<port>\d+?))?'
                   r'(?P<path>/.*?)?'
                   r'(?P<query>[?].*?)?'
                   r'$')

        regex = re.compile(pattern)
        m = regex.match(url)
        d = m.groupdict() if m is not None else None

        return d


    def parse_bean_data(self, json_data, type, name = None, pool = None):
        """
          parse mbean data and return only one request
        """
        d = {}
        for v in json_data:
            status = v.get("status")
            if(status == 200):
                d = v.get('value')
                b = d.get('mbean')

                pattern = r".*type=%s$" % (type)

                if(name != None):
                    pattern = r".*name=%s,type=%s$" % (name, type)

                if(pool != None):
                    pattern = r".*pool=%s,type=%s$" % (pool, type)

                # print(b)
                # print(pattern)

                if(re.findall(pattern, d.get('mbean'))):
                    break
                else:
                    d = {}
            else:
                d = { "status": status }

        return d


    def _portscan(self, target, port):
        try:
            # Create Socket
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            socketTimeout = 3
            s.settimeout(socketTimeout)
            s.connect((target,port))

            return True
            #print('port_scanner.is_port_opened() ' + str(port) + " is opened")
            #return port
        except socket.error as err:
            #print("{}".format(err))
            #if err.errno == ECONNREFUSED:
            #    print("ECONNREFUSED")

            return False
        except socket.error as msg:
            print( "Socket Error: %s" % msg )
        except TypeError as msg:
            print( "Type Error: %s" % msg )


    def jolokia_availability(self, jolokia_url = None, jmx_port = None ):
        """
        """

        if( jolokia_url == None):
            jolokia_url = self.jolokia_url

        if( jmx_port == None):
            jmx_port    = self.jmx_port

        jolokia_url_data = self.parse_url( jolokia_url)

        if( not self._portscan( jolokia_url_data.get('host'), int(jolokia_url_data.get('port')) ) ):
            self.logwarning( 'jolokia is currently not available' )
            return dict( { "status": 500 } )

        return dict( { 'status': 200 } )

    def read_jmx_data(self, jolokia_data, jolokia_url = None, jmx_port = None ):
        """
          jolokia_data :

          data = {
              "Server": {
                "mbean": 'com.coremedia:type=Server,application=replication-live-server',
                "attribute": [
                  'RepositorySequenceNumber',
                  'RunLevelNumeric',
                  'RunLevel']
              },
              "Replicator": {
                "mbean": 'com.coremedia:type=Replicator,application=replication-live-server',
                "attribute": [
                  'ConnectionUp',
                  'ControllerState',
                  'LatestCompletedStampedNumber',
                  'MasterLiveServerIORUrl']
              }
          }
        """

        if( jolokia_url == None):
            jolokia_url = self.jolokia_url

        if( jmx_port == None):
            jmx_port    = self.jmx_port

        jolokia_target = "service:jmx:rmi:///jndi/rmi://localhost:%d/jmxrmi" % (int(jmx_port))

        j4p = Jolokia( jolokia_url )
        # j4p.auth(httpusername='jolokia', httppassword='jolokia')
        j4p.config( ignoreErrors = 'true', ifModifiedSince = 'true', canonicalNaming = 'true' )
        j4p.target( url = jolokia_target )

        for instance in jolokia_data.keys():

            if isinstance(jolokia_data[instance], dict):
                mbean          = jolokia_data[instance]["mbean"]
                attribute_list = jolokia_data[instance]["attribute"]

                #print(mbean)
                #print(attribute_list)

                try:

                    j4p.add_request(
                        type = 'read',
                        mbean = mbean,
                        attribute = (",".join(attribute_list)) )
                except JolokiaError as error:
                    print(error)

        response = j4p.getRequests()

        # print(response)

        # rebuild dict
        for v in response:

            # print(json.dumps( v, indent = 2 ))

            status = v.get("status", 500)

            if( status == 200 ):
                data = v.get('value')
                # print(data)

                data["timestamp"] = int(v.get("timestamp"))
            else:
                v["value"] = {}
                data = v.get('value')

            data["status"] = status
            data["mbean"] = v["request"]["mbean"]

            try:
                v.pop("request")
                v.pop("stacktrace")
                v.pop("error")
            except KeyError:
                continue

        return response


    def normalize_service(self, service):
      """
        normalize service names for grafana
      """
      if(service == 'content-management-server'):
          service = 'CMS'
      elif(service == 'master-live-server'):
          service = 'MLS'
      elif(service == 'replication-live-server'):
          service = 'RLS'
      elif(service == 'workflow-server'):
          service = 'WFS'
      elif(service == 'solr-master'):
          service = 'SOLR_MASTER'
      elif(service == 'content-feeder'):
          service = 'FEEDER_CONTENT'
      elif(service == 'caefeeder-live'):
          service = 'FEEDER_LIVE'
      elif(service == 'caefeeder-preview'):
          service = 'FEEDER_PREV'
      elif(service == 'cae-preview'):
          service = 'CAE_PREV'
      elif(service == 'node-exporter'):
          service = 'NODE_EXPORTER'
      elif(service == 'http-status'):
          service = 'HTTP_STATUS'
      else:
          service

      service = service.replace('-', '_')
      service = service.upper()

      return service


    def _validate_application(self, valid_applications = [], application = None ):
        """

        """
        result  = False
        message = "'%s' is no valid application!" % (application)

        if( application == None or len(valid_applications) == 0 ):
            return result, "missing parameters"

        if application in valid_applications:
            result = True

            # cut instance number
            if re.match("^cae-live.*", application) is not None:
                application = "cae-live"

            # cut instance number
            if re.match("^replication-live-server.*", application) is not None:
                application = "replication-live-server"

            message = application

        return result, message


    def _detect_spring_boot(self):
        """

        """
        port = str(self.jmx_port)

        data = {
            # spring-boot
            "Connector": {
                "mbean": "Tomcat:type=Connector,port=%s" % ( port[:-2] + '80' ),
                "attribute": [
                  "port",
                  "stateName"]
            }
        }

        result      = self.read_jmx_data(data)
        server_data = self.parse_bean_data(result, type = "Connector")

        status = server_data.get('status')

        if( status == 200 ):
            return True

        return False


    def loginfo(self, msg):
        print("INFO: {}".format(msg))


    def logwarning(self, msg):
        print("WARNING: {}".format(msg))

    def logerror(self, msg):
        print("ERROR: {}".format(msg))

    def logverbose(self, msg):
        if self.verbose:
            print("VERBOSE: {}".format(msg))


    def logdebug(self, msg):
        if self.debug:
            print("DEBUG: {}".format(msg))

#

import sys
import re
import json
import base

class CapConnection(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def get_stats(self):
        """

        """
        result = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "workflow-server",
            "content-feeder",
            "user-changes",
            "elastic-worker",
            "caefeeder-preview",
            "caefeeder-live",
            "studio",
            "sitemanager",
            "headless-server",
            "eds",
            "cae-preview",
            "cae-live-1",
            "cae-live-2",
            "cae-live-3",
            "cae-live-4",
            "cae-live-5",
            "cae-live-6",
            "cae-live-7",
            "cae-live-8",
            "cae-live-9",
            "cae-live"]

        valid, application = self._validate_application( valid_applications,  self.jmx_application )

        if( valid == False ):
            self.logwarning( "%s is no valid application for CapConnection" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

        data = {
            "Server": {
              "mbean": 'com.coremedia:type=CapConnection,application=%s' % (application),
              "attribute": [
                'CapListRepositoryAvailable',
                'CapListRepositoryHealthy',
                'CapListRepositoryRequired',
                'ContentRepositoryAvailable',
                'ContentRepositoryHealthy',
                'ContentRepositoryRequired',
                'WorkflowRepositoryAvailable',
                'WorkflowRepositoryHealthy',
                'WorkflowRepositoryRequired',
                'LatestContentEventSequenceNumber',
                'LatestReceivedContentEventSequenceNumber',
                'BlobCacheFaults',
                'BlobCacheLevel',
                'BlobCacheSize',
                'HeapCacheFaults',
                'HeapCacheLevel',
                'HeapCacheSize',
                'NumberOfSUSessions',
                'Open',
                'Stable',
                'Url']
            }
        }

        if( self.spring_boot ):
            if( application == 'cae-live' or application == 'cae-preview' or application == 'eds' ):
                data["Server"]["mbean"] = "com.coremedia:type=CapConnection,application=blueprint"
            else:
                data["Server"]["mbean"] = "com.coremedia:type=CapConnection,application=coremedia"

        jmx_result = self.read_jmx_data(data)

        jmx_data   = self.parse_bean_data(jmx_result, "CapConnection")

        # print(json.dumps( jmx_data, indent = 2 ))

        status   = jmx_data.get("status")

        data = {}

        if(status == 200):
            """
            """
            blob_cache_size    = jmx_data.get('BlobCacheSize'  , 0)
            blob_cache_level   = jmx_data.get('BlobCacheLevel' , 0)
            blob_cache_faults  = jmx_data.get('BlobCacheFaults', 0)
            blob_cache_percent = 0

            if(blob_cache_size > 0):
              blob_cache_percent = (100 * int(blob_cache_level) / int(blob_cache_size))

            heap_cache_size    = jmx_data.get('HeapCacheSize'  , 0)
            heap_cache_level   = jmx_data.get('HeapCacheLevel' , 0)
            heap_cache_faults  = jmx_data.get('HeapCacheFaults', 0)
            heap_cache_percent = 0

            if(heap_cache_size > 0):
              heap_cache_percent = (100 * int(heap_cache_level) / int(heap_cache_size))

            su_sessions        = jmx_data.get('NumberOfSUSessions', 0)

            open_connections   = jmx_data.get('Open', 0)
            open_connections   = 1 if open_connections else 0

            content_repository = jmx_data.get('ContentRepositoryAvailable', 0)
            content_repository = 1 if content_repository else 0

            workflow_repository = jmx_data.get('WorkflowRepositoryAvailable', 0)
            workflow_repository = 1 if workflow_repository else 0

            contentevent_seq_number_latest = jmx_data.get('LatestContentEventSequenceNumber', 0)
            contentevent_seq_number_latest_received = jmx_data.get('LatestReceivedContentEventSequenceNumber', 0)


            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'blob', 'cache', 'size' ]),
              "value": blob_cache_size
            })
            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'blob', 'cache', 'used' ]),
              "value": blob_cache_level
            })
            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'blob', 'cache', 'fault' ]),
              "value": blob_cache_faults
            })
            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'blob', 'cache', 'used_percent' ]),
              "value": blob_cache_percent
            })
            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'heap', 'cache', 'size' ]),
              "value": heap_cache_size
            })
            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'heap', 'cache', 'used' ]),
              "value": heap_cache_level
            })
            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'heap', 'cache', 'fault' ]),
              "value": heap_cache_faults
            })
            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'heap', 'cache', 'used_percent' ]),
              "value": heap_cache_percent
            })
            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'su_sessions', 'sessions' ]),
              "value": su_sessions
            })
            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'open' ]),
              "value": open_connections
            })

            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'ContentRepository', 'available' ]),
              "value": content_repository
            })
            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'WorkflowRepository', 'available' ]),
              "value": workflow_repository
            })

            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'ContentEventSequenceNumber', 'Latest' ]),
              "value": contentevent_seq_number_latest
            })

            result.append( {
              "key": ".".join( [ prefix, 'CapConnection', 'ContentEventSequenceNumber', 'LatestReceived' ]),
              "value": contentevent_seq_number_latest_received
            })

        return result

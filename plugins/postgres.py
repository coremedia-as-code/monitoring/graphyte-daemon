#

from __future__ import print_function

import sys
import re
import json
import base
import time
import psycopg2

import logging

# disable log messages from the Requests library
logging.getLogger("urllib3").setLevel(logging.CRITICAL)

class Postgres(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.hostname  = kwargs.get("hostname", "127.0.0.1" )
        self.username  = kwargs.get("username", None )
        self.password  = kwargs.get("password", None )
        self.databases = kwargs.get("databases", "['postgres']" )

        self.connection = None
        self.prefix    = 'POSTGRESQL'


    def __db_connection(self, database, username, password, host = '127.0.0.1', port = 5432, connect_timeout = 10):
        """
          create database connection
        """
        connection = None

        try:
            connection = psycopg2.connect(
              user     = username,
              password = password,
              host     = host,
              port     = int(port),
              database = database,
              connect_timeout = connect_timeout)

            connection.autocommit = True

        except Exception as e:
            print("ERROR: {}".format(e))
            # assert str(e) == ""

        return connection


    def __query(self, cursor, sql, params = None):
        """
          accepts a database connection or cursor
        """
        if type(cursor) == psycopg2._psycopg.connection:
            cursor = cursor.cursor()

        # print('QUERY "{}" {}'.format(sql, params))
        try:
            if params:
                cursor.execute(sql, params)
            else:
                cursor.execute(sql)
            results = cursor.fetchall()
        except Exception:
            print("failed calling the database")
            results = []

        # print('QUERY RESULT: {} ({})'.format(results, type(results)))

        return results


    def client_connections(self, connection):
        """
        """
        result = []

        if(connection == None):
            connection = self.__db_connection( 'postgres', self.username, self.password, self.hostname )

        dba_results = self.__query(connection, 'SELECT count(*) FROM pg_stat_activity')

        if dba_results:
            result.append({
                "key": ".".join( [ self.prefix, 'connections' ]),
                "value": dba_results[0][0]
            })

        return result


    def disk_usage_for_database(self, connection):
        """
        """
        result = []

        if(connection == None):
            connection = self.__db_connection( 'postgres', self.username, self.password, self.hostname )

        dba_results = self.__query(connection, "SELECT datname, pg_database_size(datname) FROM pg_database")

        if dba_results:
            for single in dba_results:

                if(type(single) == tuple):
                    database_name, database_size = single

                    result.append({
                        "key": ".".join( [ self.prefix, 'database_size', database_name ]),
                        "value": database_size
                    })

        return result


    def transaction_rate_for_database(self, connection):
        """
        """
        result = []

        if(connection == None):
            connection = self.__db_connection( 'postgres', self.username, self.password, self.hostname )

        sql = ("SELECT datname, now(), (xact_commit + xact_rollback) as transactions, xact_rollback "
               "FROM pg_stat_database")

        dba_results = self.__query(connection, sql)

        if dba_results:
            for single in dba_results:

                if(type(single) == tuple):
                    database_name, time_now, transactions_now, rollbacks_now = single

                    result.append({
                        "key": ".".join( [ self.prefix, 'transaction_rate', database_name, 'transactions' ]),
                        "value": transactions_now
                    })
                    result.append({
                        "key": ".".join( [ self.prefix, 'transaction_rate', database_name, 'rollbacks' ]),
                        "value": rollbacks_now
                    })

        return result


    def seconds_since_last_vacuum_per_table(self, connection):
        """
          Returns a list of tuples: (db_name, table_name, seconds_since_last_vacuum)
          where seconds_since_last_vacuum is 0 if no vacuum is done ever (stays flat zero)
        """
        result = []

        if(connection == None):
            connection = self.__db_connection( 'postgres', self.username, self.password, self.hostname )

        sql = "SELECT schemaname, relname, now(), last_vacuum, last_autovacuum FROM pg_stat_user_tables"
        dba_results = self.__query(connection, sql)


        # table_last_vacuum_list = []

        for database_name, table_name, time_now, last_vacuum, last_autovacuum in dba_results:
            latest_vacuum = None
            if last_vacuum or last_autovacuum:
                latest_vacuum = max([x for x in (last_vacuum, last_autovacuum) if x])

            seconds_since_last_vacuum = int((time_now - (latest_vacuum or time_now)).total_seconds())

            result.append({
                "key": ".".join( [ self.prefix, 'autovacuum', database_name, table_name, 'seconds_since' ]),
                "value": seconds_since_last_vacuum
            })

            #print(db_name, table_name, time_now, last_vacuum, last_autovacuum)


            # table_last_vacuum_list.append((db_name, table_name, seconds_since_last_vacuum))
        # return table_last_vacuum_list

        return result


    def heap_hit_statistics(self, connection):
        """

        """
        result = []

        if(connection == None):
            connection = self.__db_connection( 'postgres', self.username, self.password, self.hostname )

        sql = "SELECT schemaname, relname, now(), sum(heap_blks_read), sum(heap_blks_hit) FROM pg_statio_user_tables"
        dba_results = self.__query(connection, sql)

        print(dba_results)

        if( dba_results ):

            database_name, time_now, heap_read_now, heap_hit_now = dba_results[0]

            result.append({
                "key": ".".join( [ self.prefix, 'statio', database_name, 'heap_read' ]),
                "value": heap_read_now
            })

            result.append({
                "key": ".".join( [ self.prefix, 'statio', database_name, 'heap_hit' ]),
                "value": heap_hit_now
            })

        return result


    def index_hit_rates(self, connection):
        """
        """
        sql = ("SELECT current_database() as db_name, relname as table_name, "
               "idx_scan as index_hit, seq_scan as index_miss "
               "FROM pg_stat_user_tables")
        results = query(conn, sql)
        index_hit_rates = []
        LOG.debug(results)
        for db_name, table_name, index_hit, index_miss in results:
            if index_hit is not None and index_miss is not None:
                if index_hit == 0:
                    recent_ratio = 0
                else:
                    recent_ratio = index_hit / float(index_miss + index_hit)
                index_hit_rates.append((db_name, table_name, recent_ratio))
            else:
                index_hit_rates.append((db_name, table_name, None))
        return index_hit_rates









    def get_stats(self):
        """

        """
        result  = []

        if(self.connection == None):
            self.connection = self.__db_connection( 'postgres', self.username, self.password, self.hostname )

        if(self.connection != None):

            result += self.client_connections(self.connection)
            result += self.disk_usage_for_database(self.connection)
            result += self.transaction_rate_for_database(self.connection)
            result += self.seconds_since_last_vacuum_per_table(self.connection)
        # self.heap_hit_statistics()

        return result

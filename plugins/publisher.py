#

import sys
import re
import json
import base

class Publisher(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def get_stats(self):
        """

        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
          "content-management-server"]

        valid, application = self._validate_application( valid_applications,  self.jmx_application )

        if( valid == False ):
            self.logwarning( "%s is no valid application for Publisher" %(self.jmx_application) )
            return []

        prefix = self.normalize_service(self.jmx_application)

        data = {
            "PublisherTarget": {
              "mbean": 'com.coremedia:type=Publisher,target=target,application=%s' % (self.jmx_application),
              "attribute": [
                'Connected',
                'PublCount',
                'PublPrevCount',
                'FailedPublCount',
                'FailedPublPrevCount',
                'LastPublResult',
                'LastPublDate',
                'LastPublSize',
                'LastPublTime',
                'LastPublWaitTime'
              ]
            }
        }

        if( self.spring_boot ):
            data["PublisherTarget"]["mbean"]    = "com.coremedia:type=Publisher,application=coremedia"

        jmx_result = self.read_jmx_data(data)

        # print(json.dumps( result, indent = 2 ))

        jmx_data = self.parse_bean_data(jmx_result, "Publisher")

        #print(json.dumps( jmx_data, indent = 2 ))

        status   = jmx_data.get("status", 404)

        data = {}

        if(status == 200):

            connected                   = jmx_data.get('Connected', False)
            queue_size                  = jmx_data.get('QueueSize', 0)
            publications                = jmx_data.get('PublCount', 0)
            publications_preview        = jmx_data.get('PublPrevCount', 0)
            failed_publications         = jmx_data.get('FailedPublCount', 0)
            failed_publications_preview = jmx_data.get('FailedPublPrevCount', 0)
            # either 'success' or 'failure' depending on whether the last completed publication was successful
            last_publications_result    = jmx_data.get('LastPublResult')
            # the time when the last publication was started, in milliseconds since 1970-01-01T00:00:00+00:00
            last_publications_date      = jmx_data.get('LastPublDate', 0)
            last_publications_size      = jmx_data.get('LastPublSize', 0.0)
            last_publications_time      = jmx_data.get('LastPublTime', 0)
            last_publications_wait_time = jmx_data.get('LastPublWaitTime', 0)

            #print(type(last_publications_size))
            if(last_publications_date == None):
                last_publications_date = 0

            if(last_publications_size == None):
                last_publications_size = 0.0

            if(last_publications_time == None):
                last_publications_time = 0

            if(last_publications_wait_time == None):
                last_publications_wait_time = 0

            # boolean: 10 == connected / 55 == N/A / 90 == not connected
            connected                   = 10 if connected else 90
            # string: 10 == success / 55 == N/A / 90 == failure
            if(last_publications_result == 'success'):
                last_publications_result    = 10
            elif(last_publications_result == 'failure'):
                last_publications_result    = 90
            else:
                last_publications_result    = 55

            result.append({
              "key": ".".join( [ prefix, 'Publisher', 'connected' ]),
              "value": connected
            })
            result.append({
              "key": ".".join( [ prefix, 'Publisher', 'queue_size' ]),
              "value": queue_size
            })
            result.append({
              "key": ".".join( [ prefix, 'Publisher', 'publications' ]),
              "value": publications
            })
            result.append({
              "key": ".".join( [ prefix, 'Publisher', 'publications_preview' ]),
              "value": publications_preview
            })
            result.append({
              "key": ".".join( [ prefix, 'Publisher', 'failed_publications' ]),
              "value": failed_publications
            })
            result.append({
              "key": ".".join( [ prefix, 'Publisher', 'failed_publications_preview' ]),
              "value": failed_publications_preview
            })
            result.append({
              "key": ".".join( [ prefix, 'Publisher', 'last_publications_result' ]),
              "value": last_publications_result
            })
            result.append({
              "key": ".".join( [ prefix, 'Publisher', 'last_publications_date' ]),
              "value": last_publications_date
            })
            result.append({
              "key": ".".join( [ prefix, 'Publisher', 'last_publications_size' ]),
              "value": last_publications_size
            })
            result.append({
              "key": ".".join( [ prefix, 'Publisher', 'last_publications_time' ]),
              "value": last_publications_time
            })
            result.append({
              "key": ".".join( [ prefix, 'Publisher', 'last_publications_wait_time' ]),
              "value": last_publications_wait_time
            })

        return result

#

import sys
import re
import json
import base

class Statistics(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def get_stats(self):
        """

        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "content-management-server",
            "master-live-server",
            "replication-live-server"]

        valid, application = self._validate_application( valid_applications,  self.jmx_application )

        if( valid == False ):
            self.logwarning( "%s is no valid application for Statistics" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

        # com.coremedia:type=Statistics,module=BlobStore,pool=BlobStoreMethods,application=master-live-server
        # com.coremedia:type=Statistics,module=RepositoryStatistics,pool=Resource,application=master-live-server
        # com.coremedia:type=Statistics,module=ResourceCacheStatistics,pool=ResourceCache,application=master-live-server
        # com.coremedia:type=Statistics,module=StoreStatistics,pool=Job Execution,application=master-live-server
        # com.coremedia:type=Statistics,module=StoreStatistics,pool=Job Failed,application=master-live-server
        # com.coremedia:type=Statistics,module=StoreStatistics,pool=Job Latency,application=master-live-server
        # com.coremedia:type=Statistics,module=StoreStatistics,pool=Job Result,application=master-live-server
        # com.coremedia:type=Statistics,module=StoreStatistics,pool=Job Successful,application=master-live-server
        # com.coremedia:type=Statistics,module=StoreStatistics,pool=Job Unrecoverable,application=master-live-server

        data = {
            "RepositoryStatistics": {
              "mbean": "com.coremedia:type=Statistics,module=RepositoryStatistics,pool=Resource,application=%s" % ( self.jmx_application ),
              "attribute": [
                'VersionCreations_sum',
                'FolderCreations_sum',
                'DocumentCreations_sum',
                'FolderRightsChecks_sum',
                'DocumentRightsChecks_sum',]
            },
            "ResourceCacheStatistics": {
              "mbean": "com.coremedia:type=Statistics,module=ResourceCacheStatistics,pool=ResourceCache,application=%s" % ( self.jmx_application ),
              "attribute": [
                'CacheSize',
                'CacheRemoved',
                'CacheFaults',
                'CacheMisses',
                'CacheHits']
            },
            "StoreStatistics": {
              "mbean": "com.coremedia:type=Statistics,module=StoreStatistics,pool=Job Result,application=%s" % ( self.jmx_application ),
              "attribute": [
                'Unrecoverable',
                'Successful',
                'Failed']
            },
        }

        if( self.spring_boot ):
            data["RepositoryStatistics"]["mbean"]    = "com.coremedia:type=Statistics,module=RepositoryStatistics,pool=Resource,application=coremedia"
            data["ResourceCacheStatistics"]["mbean"] = "com.coremedia:type=Statistics,module=ResourceCacheStatistics,pool=ResourceCache,application=coremedia"
            data["StoreStatistics"]["mbean"]         = "com.coremedia:type=Statistics,module=StoreStatistics,pool=Job Result,application=coremedia"

        jmx_result             = self.read_jmx_data(data)
        #print(json.dumps( result, indent = 2 ))

        jmx_resource_data      = self.parse_bean_data(jmx_result, type = "Statistics", pool = "Resource" )
        jmx_resourcecache_data = self.parse_bean_data(jmx_result, type = "Statistics", pool = "ResourceCache" )
        jmx_jobresult_data     = self.parse_bean_data(jmx_result, type = "Statistics", pool = "Job Result" )

        #print(json.dumps( jmx_resource_data, indent = 2 ))
        #print(json.dumps( jmx_resourcecache_data, indent = 2 ))
        #print(json.dumps( jmx_jobresult_data, indent = 2 ))

        status_resource_data      = jmx_resource_data.get("status", 404)
        status_resourcecache_data = jmx_resourcecache_data.get("status", 404)
        status_jobresult_data     = jmx_jobresult_data.get("status", 404)

        # host.MLS.StatisticsResourceCache.cache
        if(status_resource_data == 200):

            result.append({
              "key": ".".join( [ prefix, 'statistics', 'repository', 'creations', 'version' ]),
              "value": jmx_resourcecache_data.get('VersionCreations_sum', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'statistics', 'repository', 'creations', 'folder' ]),
              "value": jmx_resourcecache_data.get('FolderCreations_sum', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'statistics', 'repository', 'creations', 'document' ]),
              "value": jmx_resourcecache_data.get('DocumentCreations_sum', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'statistics', 'repository', 'rights_checks', 'folder' ]),
              "value": jmx_resourcecache_data.get('FolderRightsChecks_sum', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'statistics', 'repository', 'rights_checks', 'document' ]),
              "value": jmx_resourcecache_data.get('DocumentRightsChecks_sum', 0)
            })

        if(status_resourcecache_data == 200):

            result.append({
              "key": ".".join( [ prefix, 'statistics', 'resource_cache', 'size' ]),
              "value": jmx_resourcecache_data.get('CacheSize', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'statistics', 'resource_cache', 'removed' ]),
              "value": jmx_resourcecache_data.get('CacheRemoved', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'statistics', 'resource_cache', 'faults' ]),
              "value": jmx_resourcecache_data.get('CacheFaults', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'statistics', 'resource_cache', 'misses' ]),
              "value": jmx_resourcecache_data.get('CacheMisses', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'statistics', 'resource_cache', 'hits' ]),
              "value": jmx_resourcecache_data.get('CacheHits', 0)
            })

        if(status_jobresult_data == 200):

            result.append({
              "key": ".".join( [ prefix, 'statistics', 'job_result', 'failed' ]),
              "value": jmx_jobresult_data.get('Failed', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'statistics', 'job_result', 'successful' ]),
              "value": jmx_jobresult_data.get('Successful', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'statistics', 'job_result', 'unrecoverable' ]),
              "value": jmx_jobresult_data.get('Unrecoverable', 0)
            })

        return result

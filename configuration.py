
import yaml

class Configuration:

    config_file = None


    def read_config(self, config_file = "config.yml"):
        """
        Take a list of yaml_files and load them to return back
        to the testing program
        """
        loaded_yaml = []

        try:
            with open(config_file, 'r') as fd:
                loaded_yaml.append(yaml.safe_load(fd))
        except IOError as e:
            print('Error reading file', config_file)
            raise e
        except yaml.YAMLError as e:
            print('Error parsing file', config_file)
            raise e
        except Exception as e:
            print('General error')
            raise e

        #print(type(loaded_yaml))
        #print(type(loaded_yaml[0]))

        return loaded_yaml[0]

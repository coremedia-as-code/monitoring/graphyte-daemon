#!/usr/bin/env python
#
# vim: tabstop=4 shiftwidth=4

import sys
import re
import base
import traceback
import subprocess
import json

from datetime import datetime

class License(base.Base):
    """

    """
    host     = ''
    jmx_port = 0

    CMS_PORT = 40199
    MLS_PORT = 40299
    RLS_PORT = 42099

    LIC_VALID_FROM = 1
    LIC_UNTIL_HARD = 2
    LIC_UNTIL_SOFT = 3


    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    names = {
      1: 'from',
      2: 'hard',
      3: 'soft'
    }

# -------------------------------------------------------------------------------------------------

    def get_stats(self):
        """
          Retrieves stats regarding latency to write to a test pool
        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
          "content-management-server",
          "master-live-server",
          "replication-live-server"]

        valid, application = self._validate_application( valid_applications,  self.jmx_application )

        if( valid == False ):
            self.logwarning( "%s is no valid application for License" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

        data = {
            "Server": {
              "mbean": "com.coremedia:type=Server,application=%s" % ( self.jmx_application ),
              "attribute": [
                'LicenseValidUntilHard',
                'LicenseValidUntilSoft',
                'ServiceInfos']
            }
        }

        if( self.spring_boot ):
            data["Server"]["mbean"]    = "com.coremedia:type=Server,application=coremedia"

        result   = self.read_jmx_data(data)
        jmx_data = self.parse_bean_data(result, "Server")
        #print(json.dumps( jmx_data, indent = 2 ))
        status   = jmx_data.get("status", 404)
        result   = []

        if(status == 200):
            """
            """
            service_infos = jmx_data.get("ServiceInfos")

            for elem in service_infos.keys():

                s               = service_infos.get(elem)
                enabled         = s.get('enabled', False)

                if(enabled == False):
                    continue

                named           = s.get('named', 0)
                named_max       = s.get('maxnamed', 0)
                named_diff      = named_max - named
                concurrent      = s.get('concurrent', 0)
                concurrent_max  = s.get('maxconcurrent', 0)
                concurrent_diff = concurrent_max - concurrent

                # print(json.dumps( s, indent = 2 ))

                result.append({
                    "key": ".".join( [ prefix, 'Server', 'ServiceInfo', elem, 'named' ]),
                    "value": named
                })
                result.append({
                    "key": ".".join( [ prefix, 'Server', 'ServiceInfo', elem, 'named', 'max'  ]),
                    "value": named_max
                })
                result.append({
                    "key": ".".join( [ prefix, 'Server', 'ServiceInfo', elem, 'named', 'diff' ]),
                    "value": named_diff
                })
                result.append({
                    "key": ".".join( [ prefix, 'Server', 'ServiceInfo', elem, 'concurrent' ]),
                    "value": concurrent
                })
                result.append({
                    "key": ".".join( [ prefix, 'Server', 'ServiceInfo', elem, 'concurrent', 'max' ]),
                    "value": concurrent_max
                })
                result.append({
                    "key": ".".join( [ prefix, 'Server', 'ServiceInfo', elem, 'concurrent', 'diff' ]),
                    "value": concurrent_diff
                })


            for lic_type in [self.LIC_UNTIL_SOFT, self.LIC_UNTIL_HARD]:

                difference, years, month, weeks, days, seconds = self.parse_license_date(jmx_data, lic_type)

                lic_type_named = self.names[lic_type]

                result.append({
                  "key": ".".join( [ prefix, 'Server', 'License', 'until', lic_type_named, 'month' ]),
                  "value": month
                })
                result.append( {
                  "key": ".".join( [ prefix, 'Server', 'License', 'until', lic_type_named, 'weeks' ]),
                  "value": weeks
                })
                result.append({
                  "key": ".".join( [ prefix, 'Server', 'License', 'until', lic_type_named, 'days' ]),
                  "value": days
                })

        return result


    def parse_license_date( self, data, lic_type ):
        """

        """
        if( lic_type == self.LIC_UNTIL_HARD ):
          raw_date = data['LicenseValidUntilHard']
        elif( lic_type == self.LIC_UNTIL_SOFT ):
          raw_date = data['LicenseValidUntilSoft']
        else:
          raw_date = data['LicenseValidFrom']

        current_datetime = datetime.now()
        license_datetime = datetime.fromtimestamp( raw_date / 1000 )

        difference       = license_datetime - current_datetime
        duration_in_s    = difference.total_seconds()

        years            = divmod(duration_in_s, 31556952)[0]    # Seconds in a year = 365*24*60*60 = 31536000.
        month            = divmod(duration_in_s, 2628288)[0]
        weeks            = divmod(duration_in_s, 604800)[0]
        days             = divmod(duration_in_s, 86400)
        hours            = divmod(days[1], 3600)
        minutes          = divmod(hours[1], 60)

        return (license_datetime - current_datetime), years, month, weeks, days[0], duration_in_s



# -------------------------------------------------------------------------------------------------

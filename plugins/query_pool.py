#

import sys
import re
import json
import base

class QueryPool(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def get_stats(self):
        """

        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "content-management-server",
            "master-live-server",
            "replication-live-server",
            "workflow-server"]

        valid, application = self._validate_application( valid_applications,  self.jmx_application )

        if( result == False ):
            self.logwarning( "%s is no valid application for QueryPool" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

        data = {
            "Server": {
              "mbean": "com.coremedia:type=Store,bean=QueryPool,application=%s" % ( self.jmx_application ),
              "attribute": [
                'RunningExecutors',
                'IdleExecutors',
                'MaxQueries',
                'WaitingQueries']
            }
        }

        if( self.spring_boot ):
            data["Server"]["mbean"]    = "com.coremedia:type=Store,bean=QueryPool,application=coremedia"

        jmx_result   = self.read_jmx_data(data)
        #print(json.dumps( jmx_result, indent = 2 ))
        jmx_data = self.parse_bean_data(jmx_result, "Store")
        #print(json.dumps( jmx_data, indent = 2 ))
        status   = jmx_data.get("status", 404)

        if(status == 200):

            result.append({
              "key": ".".join( [ prefix, 'QueryPool', 'executors', 'running' ]),
              "value": jmx_data.get('RunningExecutors', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'QueryPool', 'executors', 'idle' ]),
              "value": jmx_data.get('IdleExecutors', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'QueryPool', 'queries', 'max' ]),
              "value": jmx_data.get('MaxQueries', 0)
            })
            result.append({
              "key": ".".join( [ prefix, 'QueryPool', 'queries', 'waiting' ]),
              "value": jmx_data.get('WaitingQueries', 0)
            })

        return result

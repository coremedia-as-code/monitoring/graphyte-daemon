#!/usr/bin/env python
#
# vim: tabstop=4 shiftwidth=4

import sys
import re
import base
import json

from datetime import datetime

class ContentServer(base.Base):
    """

    """

    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


# -------------------------------------------------------------------------------------------------

    def get_stats(self):
        """
          Retrieves stats regarding latency to write to a test pool
        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
          "content-management-server",
          "master-live-server",
          "replication-live-server"]

        valid, application = self._validate_application( valid_applications,  self.jmx_application )

        if( valid == False ):
            self.logwarning( "%s is no valid application for ContentServer" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

        data = {
            "Server": {
              "mbean": "com.coremedia:type=Server,application=%s" % ( self.jmx_application ),
              "attribute": [
                'ResourceCacheHits',
                'ResourceCacheEvicts',
                'ResourceCacheEntries',
                'ResourceCacheInterval',
                'ResourceCacheSize',
                'RepositorySequenceNumber',
                'RunLevel',               # the current run level of the Content Server, which is one of 'offline', 'maintenance', 'administration', or 'online'
                'RunLevelNumeric',        # the current run level of the Content Server, which is one of 0='offline', 1='maintenance', 2='administration', or 3='online'
                'ConnectionCount',
                'Uptime']                 # the time since the Content Server was started in milliseconds
            }
        }

        if( self.spring_boot ):
            data["Server"]["mbean"]           = "com.coremedia:type=Server,application=coremedia"

        jmx_result  = self.read_jmx_data(data)
        jmx_data    = self.parse_bean_data(jmx_result, "Server")
        #print(json.dumps( jmx_data, indent = 2 ))
        status      = jmx_data.get("status", 404)

        if(status == 200):
            """
            """
            result.append({
                "key": ".".join( [ prefix, 'Server', 'ResourceCache', 'hits' ]),
                "value": jmx_data.get("ResourceCacheHits", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Server', 'ResourceCache', 'evicts' ]),
                "value": jmx_data.get("ResourceCacheEvicts", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Server', 'ResourceCache', 'entries' ]),
                "value": jmx_data.get("ResourceCacheEntries", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Server', 'ResourceCache', 'interval' ]),
                "value": jmx_data.get("ResourceCacheInterval", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Server', 'ResourceCache', 'size' ]),
                "value": jmx_data.get("ResourceCacheSize", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Server', 'uptime' ]),
                "value": jmx_data.get("Uptime", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Server', 'connection' ]),
                "value": jmx_data.get("ConnectionCount", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Server', 'runlevel_numeric' ]),
                "value": jmx_data.get("RunLevelNumeric", 0)
            })

            runlevel = jmx_data.get("RunLevel", 0).lower()

            if(runlevel == 'offline'):
                runlevel = 0
            elif(runlevel == 'online'):
                runlevel = 1
            elif(runlevel == 'administration'):
                runlevel = 11
            else:
                runlevel = 0

            result.append({
                "key": ".".join( [ prefix, 'Server', 'runlevel' ]),
                "value": runlevel
            })

            result.append({
                "key": ".".join( [ prefix, 'Server', 'Repository', 'SequenceNumber' ]),
                "value": jmx_data.get("RepositorySequenceNumber", 0)
            })

        return result

# -------------------------------------------------------------------------------------------------

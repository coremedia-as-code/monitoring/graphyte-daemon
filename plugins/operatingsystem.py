#

from __future__ import print_function

import sys
import os
import re
import json
import base
import time
import collections
import psutil
from psutil._common import bytes2human

class OperatingSystem(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.prefix = 'OPERATING_SYSTEM'


    def __cpu(self):
        """
        """
        result = []

        num_cpus         = psutil.cpu_count()

        cpus_percent     = psutil.cpu_percent(interval = None, percpu = True)
        cpus_percent_sum = psutil.cpu_percent(interval = None)

        result.append({
            "key": ".".join( [ self.prefix, 'cpu_all', 'percent_usage' ]),
            "value": cpus_percent_sum
        })

        for core in range(num_cpus):
            result.append({
                "key": ".".join( [ self.prefix, 'cpu_' + str(core), 'percent_usage' ]),
                "value": cpus_percent.pop(0)
            })

        # ------------------------------------------------------------------------------------------------------------
        #
        user, nice, system, idle, iowait, irq, softirq, steal, guest, guest_nice = psutil.cpu_times( percpu = False )
        cpu_times = list(
          map(
            lambda d: { 'user': d.user, 'system': d.system, 'idle': d.idle, 'iowait': d.iowait },
            psutil.cpu_times(percpu = True)
          )
        )

        result.append({
            "key": ".".join( [ self.prefix, 'cpu_all', 'times', 'user' ]),
            "value": user
        })
        result.append({
            "key": ".".join( [ self.prefix, 'cpu_all', 'times', 'system' ]),
            "value": system
        })
        result.append({
            "key": ".".join( [ self.prefix, 'cpu_all', 'times', 'idle' ]),
            "value": idle
        })
        result.append({
            "key": ".".join( [ self.prefix, 'cpu_all', 'times', 'iowait' ]),
            "value": iowait
        })

        for core in range(num_cpus):
            data = cpu_times[core]
            for k,v in data.items():
                result.append({
                    "key": ".".join( [ self.prefix, 'cpu_' + str(core), 'times', k ]),
                    "value": v
                })

        # ------------------------------------------------------------------------------------------------------------
        #
        current, min, max = psutil.cpu_freq(percpu = False)
        cpu_freqs = list(
          map(
            lambda d: { 'current': d.current, 'min': d.min, 'max': d.max },
            psutil.cpu_freq(percpu = True)
          )
        )

        result.append({
            "key": ".".join( [ self.prefix, 'cpu_all', 'frequency', 'current' ]),
            "value": current
        })
        result.append({
            "key": ".".join( [ self.prefix, 'cpu_all', 'frequency', 'min' ]),
            "value": min
        })
        result.append({
            "key": ".".join( [ self.prefix, 'cpu_all', 'frequency', 'max' ]),
            "value": max
        })

        for core in range(num_cpus):
            data = cpu_freqs[core]
            for k,v in data.items():
                result.append({
                    "key": ".".join( [ self.prefix, 'cpu_' + str(core), 'frequency', k ]),
                    "value": v
                })

        # ------------------------------------------------------------------------------------------------------------
        #
        load1, load5, load15 = psutil.getloadavg()
        percent_load1, percent_load5, percent_load15 = [x / num_cpus * 100 for x in psutil.getloadavg()]

        result.append({
            "key": ".".join( [ self.prefix, 'load', '1min' ]),
            "value": load1
        })
        result.append({
            "key": ".".join( [ self.prefix, 'load', '5min' ]),
            "value": load5
        })
        result.append({
            "key": ".".join( [ self.prefix, 'load', '15min' ]),
            "value": load15
        })

        result.append({
            "key": ".".join( [ self.prefix, 'load', 'percent_1min' ]),
            "value": percent_load1
        })
        result.append({
            "key": ".".join( [ self.prefix, 'load', 'percent_5min' ]),
            "value": percent_load5
        })
        result.append({
            "key": ".".join( [ self.prefix, 'load', 'percent_15min' ]),
            "value": percent_load15
        })

        return result


    def __memory(self):
        """
        """
        result = []

        def pprint_ntuple(nt, desc = 'memory' ):

            result = []

            for name in nt._fields:
                value = getattr(nt, name)

                result.append({
                    "key": ".".join( [ self.prefix, desc, name ]),
                    "value": value
                })

            return result


        memory = psutil.virtual_memory()
        swap   = psutil.swap_memory()

        result += pprint_ntuple(memory)
        result += pprint_ntuple(swap, 'swap')

        return result


    def __disc(self):
        """
        """
        result = []

        def pprint_ntuple(nt, desc = ['io_sum'] ):

            result = []

            for name in nt._fields:

                if('merged' in name):
                    continue

                value = getattr(nt, name)
                result.append({
                    "key": ".".join( [ self.prefix, 'disc' ] + desc + [ name ]),
                    "value": value
                })

            return result



        for part in psutil.disk_partitions(all=False):
            """
            """
            try:
                usage = psutil.disk_usage( part.mountpoint )
            except OSError as e:
                print( u'Caught exception when crawling disk_usage: {0}'.format(e))


            if( part.mountpoint == '/' ):
                mountpoint = 'rootfs'
            else:
                mountpoint = part.mountpoint[1:]

            result.append({
                "key": ".".join( [ self.prefix, 'disc', 'mountpoint', mountpoint.replace('/','-'), 'total' ]),
                "value": usage.total
            })
            result.append({
                "key": ".".join( [ self.prefix, 'disc', 'mountpoint', mountpoint.replace('/','-'), 'used' ]),
                "value": usage.used
            })
            result.append({
                "key": ".".join( [ self.prefix, 'disc', 'mountpoint', mountpoint.replace('/','-'), 'free' ]),
                "value": usage.free
            })
            result.append({
                "key": ".".join( [ self.prefix, 'disc', 'mountpoint', mountpoint.replace('/','-'), 'percent' ]),
                "value": int(usage.percent)
            })

        # read_count, write_count, read_bytes, write_bytes, read_time, write_time = ps.disk_io_counters()
        result += pprint_ntuple( psutil.disk_io_counters() )

        try:
            disk_counters = psutil.disk_io_counters(perdisk=True)

            for device_name in disk_counters:

                result += pprint_ntuple( disk_counters[device_name], ['io', device_name ] )

        except OSError as e:
            print( u'Caught exception when crawling disk I/O counters: {0}'.format(e))


        return result


    def __network(self):
        """
        """
        result = []

        network_info = psutil.net_io_counters(pernic=True, nowrap=False)

        for k, v in network_info.items():
            """
            """
            result.append({
                "key": ".".join( [ self.prefix, 'network', k, 'bytes', 'sent' ]),
                "value": v.bytes_sent
            })
            result.append({
                "key": ".".join( [ self.prefix, 'network', k, 'bytes', 'recv' ]),
                "value": v.bytes_recv
            })
            result.append({
                "key": ".".join( [ self.prefix, 'network', k, 'packets', 'sent' ]),
                "value": v.packets_sent
            })
            result.append({
                "key": ".".join( [ self.prefix, 'network', k, 'packets', 'recv' ]),
                "value": v.packets_recv
            })

        network_info = psutil.net_io_counters()

        result.append({
            "key": ".".join( [ self.prefix, 'network', 'total', 'bytes', 'sent' ]),
            "value": network_info.bytes_sent
        })
        result.append({
            "key": ".".join( [ self.prefix, 'network', 'total', 'bytes', 'recv' ]),
            "value": network_info.bytes_recv
        })
        result.append({
            "key": ".".join( [ self.prefix, 'network', 'total', 'packets', 'sent' ]),
            "value": network_info.packets_sent
        })
        result.append({
            "key": ".".join( [ self.prefix, 'network', 'total', 'packets', 'recv' ]),
            "value": network_info.packets_recv
        })

        return result


    def get_stats(self):
        """

        """
        result        = []

        result.append({
            "key": ".".join( [ self.prefix, 'uptime' ]),
            "value": round(time.time() - psutil.boot_time(), 2)
        })

        # prefix        = self.normalize_service('')

        result += self.__cpu()
        result += self.__memory()
        result += self.__disc()
        result += self.__network()

        return result

# https://stackoverflow.com/questions/276052/how-to-get-current-cpu-and-ram-usage-in-python#2468983
# http://code.activestate.com/recipes/577972-disk-usage/
# https://stackoverflow.com/questions/31860163/how-to-get-disk-io-and-network-usage-as-percent-by-psutil#31860824

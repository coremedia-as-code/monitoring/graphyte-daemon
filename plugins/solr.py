#

from __future__ import print_function

import sys
import re
import json
import base
# import requests
from requests import get
from requests import RequestException, ConnectionError, URLRequired, TooManyRedirects, Timeout

import logging

# disable log messages from the Requests library
logging.getLogger("urllib3").setLevel(logging.CRITICAL)

class Solr(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )

        self.solr_url         = kwargs.get("solr_url", "http://localhost:40080")

        # https://lucene.apache.org/solr/guide/8_4/metrics-reporting.html#metrics-api
        self.admin_metrics    = "/solr/admin/metrics?wt=json&type=counter,gauge&group=core"
        self.admin_cores_path = "/solr/admin/cores?action=STATUS&wt=json"

        #self.spring_boot     = self._detect_spring_boot()


    def __request_data(self, url, path):
      """
        executes get request to retrieve data from given url
      """

      logging.captureWarnings(True)

      try:
          response = get(url + path)
          # check response content type to determine which actuator api to be used
          if response.ok:
              return 200, response, None
          else:
              ex = str(Exception(response.status_code, url))
              print(ex)
              return 404, Exception(response.status_code, url), None

      except ConnectionRefusedError as e:
          logging.error('Failed to establish a new connection: {} '.format(url) )
          return 404, None, msg

      except ConnectionError as e:
          msg = 'error fetching data from {}' . format(url)
          #logging.error('error fetching data from {}'.format(url))
          #logging.debug('error fetching data from {}'.format(url))
          return 404, None, msg

      finally:
          logging.captureWarnings(False)


    def __get_solr_cores(self, prefix):
        """
        """
        result   = []

        status, response, error_message = self.__request_data(self.solr_url, self.admin_cores_path)

        if(status == 200):
            """
            """
            result = []

            content_type = response.headers['Content-Type']
            content_body = response.text

            data = json.loads(content_body)

            core_state = data.get("status")

            # head or status information for every core
            #
            for solr_core in core_state.keys():
                # print(solr_core)

                d = core_state.get(solr_core)

                #print(json.dumps( d, indent = 2 ))

                core_uptime     = d.get("uptime")

                result.append({
                    "key": ".".join( [ prefix, "core_" + solr_core, 'uptime' ]),
                    "value": core_uptime
                })

                core_index      = d.get("index")

                if( not core_index == None):
                    core_num_docs   = core_index.get("numDocs")
                    core_max_docs   = core_index.get("maxDoc")
                    core_del_docs   = core_index.get("deletedDocs")
                    core_index_version = core_index.get("version")
                    core_size       = core_index.get("sizeInBytes")

                    result.append({
                        "key": ".".join( [ prefix, "core_" + solr_core, 'size' ]),
                        "value": core_size
                    })
                    result.append({
                        "key": ".".join( [ prefix, "core_" + solr_core, 'documents', 'current' ]),
                        "value": core_num_docs
                    })
                    result.append({
                        "key": ".".join( [ prefix, "core_" + solr_core, 'documents', 'max' ]),
                        "value": core_max_docs
                    })
                    result.append({
                        "key": ".".join( [ prefix, "core_" + solr_core, 'documents', 'deleted' ]),
                        "value": core_del_docs
                    })
                    result.append({
                        "key": ".".join( [ prefix, "core_" + solr_core, 'documents', 'index_version' ]),
                        "value": core_index_version
                    })

        return result


    def __get_solr_metrics(self, prefix):
        """
        """
        result   = []

        status, response, error_message = self.__request_data(self.solr_url, self.admin_metrics)

        if(status == 200):
            """
            """
            content_type = response.headers['Content-Type']
            content_body = response.text

            data = json.loads(content_body)

            # print(json.dumps( data, indent = 2 ))

            core_metrics = data.get("metrics")

            for solr_core in core_metrics.keys():

                d = core_metrics.get(solr_core)

                # print(json.dumps( d, indent = 2 ))

                core_name  = "core_%s" % d.get("CORE.coreName")
                core_name  = core_name.lower()

                # complexer metrix
                #  - remove '/'
                metric = [
                  "CORE.fs.totalSpace",
                  "CORE.fs.usableSpace",
                  "INDEX.sizeInBytes",
                  "SEARCHER.searcher.deletedDocs",
                  "SEARCHER.searcher.maxDoc",
                  "SEARCHER.searcher.numDocs",
                  "SEARCHER.searcher.warmupTime",
                  "UPDATE.update.requests",
                  "UPDATE.update.totalTime",
                  "UPDATE.updateHandler.adds",
                  "UPDATE.updateHandler.autoCommits",
                  "UPDATE.updateHandler.deletesById",
                  "UPDATE.updateHandler.deletesByQuery",
                  "UPDATE.updateHandler.docsPending",
                  "UPDATE.updateHandler.errors",
                  "UPDATE.updateHandler.softAutoCommits",
                  "REPLICATION./replication.requests.count",
                  "REPLICATION./replication.isMaster",
                  "REPLICATION./replication.isSlave",
                  "REPLICATION./replication.replicationEnabled",
                  "REPLICATION./replication.generation",
                  "REPLICATION./replication.requests",
                  "REPLICATION./replication.totalTime"]

                for m in metric:
                    v = d.get(m)
                    m = m.replace('/', '') # .split('.')[1:]

                    if(v == True):
                        value = 1
                    elif(v == False):
                        value = 0
                    elif(v == None):
                        value = 0
                    else:
                        value = v

                    result.append({
                        "key": ".".join( [ prefix, core_name, m ] ),
                        "value": value
                    })


                # more complexer
                # read from an dict
                metric = [
                  "CACHE.searcher.documentCache",
                  "CACHE.searcher.fieldValueCache",
                  "CACHE.searcher.filterCache",
                  "CACHE.searcher.perSegFilter",
                  "CACHE.searcher.queryResultCache"
                  ]

                for m in metric:
                    data = d.get(m)

                    for k,v in data.items():

                        # m = self.__rename_upper_identifier(m)

                        result.append({
                            "key": ".".join( [ prefix, core_name, m, k ]),
                            "value": v
                        })

        return result


    def __rename_upper_identifier(self, s):
        return ".".join([ x.lower() if x.isupper() else x for x in s.split('.') ])


    def get_stats(self):
        """

        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "solr",
            "solr-master",
            "solr-slave"]

        valid, _ = self._validate_application( valid_applications,  self.jmx_application )

        if( valid == False ):
            self.logwarning( "%s is no valid application for Solr" %(self.jmx_application) )
            return []

        prefix         = self.normalize_service(self.jmx_application)

        core_result    = self.__get_solr_cores(prefix)
        metrics_result = self.__get_solr_metrics(prefix)

        result =  core_result + metrics_result

        return result

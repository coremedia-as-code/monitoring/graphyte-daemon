#!/usr/bin/env python
#
# vim: tabstop=4 shiftwidth=4

import sys
import re
import base
import json

from datetime import datetime, timedelta

class SequenceNumber(base.Base):
    """

    """

    MLS_PORT = 40299
    RLS_PORT = 42099

    RUNLEVEL_OFFLINE = 0
    RUNLEVEL_MAINTENANCE = 1
    RUNLEVEL_ADMINISTRATION = 2
    RUNLEVEL_ONLINE = 3

    CONTROLER_STATE_FAILED = 0
    CONTROLER_STATE_NOTSTARTED = 1
    CONTROLER_STATE_STOPPED = 2
    CONTROLER_STATE_RUNNING = 5

    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    # -> STRING   Thu Jan 01 00:00:00 GMT 1970
    # <- DATETIME 1970-01-01 01:00:00
    def __convert_to_datetime(self, datetime_string):
        """
        """
        # split on spaces
        ts = datetime_string.split()
        # remove the timezone
        tz = ts.pop(4)
        # parse the timezone to minutes and seconds
        tz_offset = -1 *60 # int(tz[-6] + str(int(tz[-5:-3]) * 60 + int(tz[-2:])))
        # return a datetime that is offset

        dt = datetime.strptime(' '.join(ts), '%a %b %d %H:%M:%S %Y') - timedelta(minutes=tz_offset)
        return int(dt.timestamp())


# -------------------------------------------------------------------------------------------------

    def get_stats(self):
        """
          Retrieves stats regarding latency to write to a test pool
        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "replication-live-server"]

        valid, application = self._validate_application( valid_applications,  self.jmx_application )

        if( valid == False ):
            self.logwarning( "%s is no valid application for SequenceNumber" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

        data = {
            "Server": {
              "mbean": "com.coremedia:type=Server,application=%s" % ( self.jmx_application ),
              "attribute": [
                'RepositorySequenceNumber',
                'RunLevelNumeric',        # the current run level of the Content Server, which is one of 0='offline', 1='maintenance', 2='administration', or 3='online'
                'Uptime']                 # the time since the Content Server was started in milliseconds
            },
            "Replicator": {
              "mbean": "com.coremedia:type=Replicator,application=%s" % ( self.jmx_application ),
              "attribute": [
                'ConnectionUp',                  # bool   | whether the connection to the master live server is up
                'PipelineUp',                    # bool   | whether the replicator event processing pipeline is up and running
                'Enabled',                       # bool   | whether the continuous replication of content changes is enabled. Corresponds to property replicator.enable
                'ControllerState',               # string | the current state of the replicator, one of: "failed", "stopped", "running", "not started"
                'UncompletedCount',              # int    | The number of events to be processed (the difference between the number of incoming events and completed events)
                'CompletedCount',                # int    | Total number of events that have been completed since server startup
                'LatestIncomingSequenceNumber',  # int    | Sequence number of the latest incoming event
                'LatestIncomingStampedNumber',   # int    | Sequence number of the latest incoming StampedEvent (indicates the end of a publication)
                'LatestCompletedSequenceNumber', # int    | Sequence number of the latest completed event
                'LatestCompletedStampedNumber',  # int    | Sequence number of the latest completion of a StampedEvent (indicates the end of a publication)
                'CompletedStartTime',            # string | Date of the first completion of an event since server startup
                'IncomingStartTime',             # string | Date of the first arrival of an incoming event since server startup
                'LatestCompletedArrival',        # string | Date of the latest completion of an event
                'LatestIncomingArrival',         # string | Date of the latest arrival of an incoming event
                'LatestCompletionDuration',      # string | Difference in milliseconds between the arrival and the completion of the latest completed event since server startup
                'MasterLiveServerIORUrl'         # string | Master Liver Server IOR Url (e.g. http://mls.cm.local:40280/master-live-server/ior)
              ]
            }
        }

        if( self.spring_boot ):
            data["Server"]["mbean"]     = "com.coremedia:type=Server,application=coremedia"
            data["Replicator"]["mbean"] = "com.coremedia:type=Replicator,application=coremedia"

        jmx_result      = self.read_jmx_data(data)
        jmx_server_data = self.parse_bean_data(jmx_result, "Server")
        jmx_replic_data = self.parse_bean_data(jmx_result, "Replicator")

        #print(json.dumps( jmx_server_data, indent = 2 ))
        #print(json.dumps( jmx_replic_data, indent = 2 ))
        server_status   = jmx_server_data.get("status", 404)
        replic_status   = jmx_replic_data.get("status", 404)

        if(replic_status == 200):
            """
            """
            pipleline_up               = jmx_replic_data.get('PipelineUp', False)
            enabled                    = jmx_replic_data.get('Enabled', False)
            controler_state            = jmx_replic_data.get('ControllerState', 'failed')
            controler_state_numeric    = 0

            completed_start_time       = jmx_replic_data.get('CompletedStartTime')
            incoming_start_time        = jmx_replic_data.get('IncomingStartTime')
            latest_completed_arrival   = jmx_replic_data.get('LatestCompletedArrival')
            latest_incoming_arrival    = jmx_replic_data.get('LatestIncomingArrival')
            latest_completion_duration = jmx_replic_data.get('LatestCompletionDuration')

            completed_start_time       = self.__convert_to_datetime( completed_start_time )
            incoming_start_time        = self.__convert_to_datetime( incoming_start_time )
            latest_completed_arrival   = self.__convert_to_datetime( latest_completed_arrival )
            latest_incoming_arrival    = self.__convert_to_datetime( latest_incoming_arrival )

            if(controler_state == 'failed'):
                controler_state_numeric = self.CONTROLER_STATE_FAILED
            elif(controler_state == 'not sarted'):
                controler_state_numeric = self.CONTROLER_STATE_NOTSTARTED
            elif(controler_state == 'stopped'):
                controler_state_numeric = self.CONTROLER_STATE_STOPPED
            elif(controler_state == 'running'):
                controler_state_numeric = self.CONTROLER_STATE_RUNNING
            else:
                controler_state_numeric = self.CONTROLER_STATE_FAILED



            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'PipelineUp' ]),
                "value": int(pipleline_up)
            })
            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'Enabled' ]),
                "value": int(enabled)
            })
            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'ControllerState' ]),
                "value": controler_state_numeric
            })
            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'Events', 'CompletedStartTime' ]),
                "value": completed_start_time
            })
            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'Events', 'IncomingStartTime' ]),
                "value": incoming_start_time
            })
            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'Events', 'LatestCompletedArrival' ]),
                "value": latest_completed_arrival
            })
            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'Events', 'LatestIncomingArrival' ]),
                "value": latest_incoming_arrival
            })
            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'Events', 'LatestCompletionDuration' ]),
                "value": latest_completion_duration
            })
            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'LatestCompleted', 'StampedNumber' ]),
                "value": jmx_replic_data.get("LatestCompletedStampedNumber", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'LatestCompleted', 'SequenceNumber' ]),
                "value": jmx_replic_data.get("LatestCompletedSequenceNumber", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'LatestIncoming', 'StampedNumber' ]),
                "value": jmx_replic_data.get("LatestIncomingStampedNumber", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'LatestIncoming', 'SequenceNumber' ]),
                "value": jmx_replic_data.get("LatestIncomingSequenceNumber", 0)
            })

            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'Events', 'Uncompleted' ]),
                "value": jmx_replic_data.get("UncompletedCount", 0)
            })
            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'Events', 'Completed' ]),
                "value": jmx_replic_data.get("CompletedCount", 0)
            })


        if(server_status == 200 and replic_status == 200):
            """

            """
            rls_runlevel          = jmx_server_data.get('RunLevelNumeric')

            mls_ior               = jmx_replic_data.get('MasterLiveServerIORUrl')
            connected_to_mls      = jmx_replic_data.get('ConnectionUp')
            latest_stamped_number = jmx_replic_data.get('LatestCompletedStampedNumber')

            mls_url_data          = self.parse_url(mls_ior)
            mls_host              = mls_url_data.get('host')

            mls_data              = self.__mls_data(mls_host)
            mls_server            = self.parse_bean_data(mls_data, "Server")

            mls_status            = mls_server.get("status")
            mls_runlevel          = mls_server.get('RunLevelNumeric')
            repository_seq_number = mls_server.get('RepositorySequenceNumber')

            result.append({
                "key": ".".join( [ prefix, 'Replicator', 'ConnectionUp' ]),
                "value": int(connected_to_mls)
            })

            if( mls_status == 200 and mls_runlevel == self.RUNLEVEL_ONLINE ):

                diff_sequence_number = repository_seq_number - latest_stamped_number

                # graphyte.$host.RLS.SequenceNumber.diffToMLS
                result.append({
                  "key": ".".join( [ prefix, 'SequenceNumber', 'diffToMLS' ]),
                  "value": diff_sequence_number
                })


        return result

# -------------------------------------------------------------------------------------------------

    def __mls_data(self, mls_host):
        """

        """

        data = {
            "Server": {
              "mbean": 'com.coremedia:type=Server,application=master-live-server',
              "attribute": [
                'RepositorySequenceNumber',
                'RunLevelNumeric',
                'RunLevel']
            }
        }

        if( self.spring_boot ):
            data["Server"]["mbean"]     = "com.coremedia:type=Server,application=coremedia"

        jolokia_url = "http://%s:8080/jolokia/" % ( mls_host )

        # TODO
        # function read_jmx_data() has no host parameter!
        return self.read_jmx_data(data, jolokia_url, self.MLS_PORT)

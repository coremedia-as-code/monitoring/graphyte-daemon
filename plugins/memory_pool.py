#

import sys
import re
import json
import base

class MemoryPool(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )


    def get_stats(self):
        """

        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "content-management-server",
            "master-live-server",
            "replication-live-server",
            "workflow-server",
            "caefeeder-preview",
            "caefeeder-live",
            "content-feeder",
            "user-changes",
            "elastic-worker",
            "studio",
            "solr",
            "solr-master",
            "solr-slave",
            "cae-preview",
            "headless-server",
            "eds",
            "cae-live-1",
            "cae-live-2",
            "cae-live-3",
            "cae-live-4",
            "cae-live-5",
            "cae-live-6",
            "cae-live-7",
            "cae-live-8",
            "cae-live-9",
            "cae-live"]

        valid, application = self._validate_application( valid_applications,  self.jmx_application )

        if( valid == False ):
            self.logwarning( "%s is no valid application for MemoryPool" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

        memory_pool_types = [
            'CMS Old Gen',
            'Code Cache',
            'Compressed Class Space',
            'Metaspace',
            'Par Eden Space',
            'Par Survivor Space',
            "G1 Survivor Space",
            "G1 Old Gen",
            "G1 Eden Space",
            "PS Eden Space",
            "PS Old Gen",
            "PS Survivor Space",
            "CodeHeap 'profiled nmethods'",
            "CodeHeap 'non-profiled nmethods'",
            "CodeHeap 'non-nmethods'"
        ]

        data= {}
        for memory_type in memory_pool_types:

            identifier = "MemoryPool_%s" %(memory_type.upper())

            data[identifier] = {
                  "mbean": 'java.lang:type=MemoryPool,name=%s' % (memory_type),
                  "attribute": [
                      "Usage"]
              }

        jmx_result   = self.read_jmx_data(data)

        # print(json.dumps( jmx_result, indent = 2 ))

        result   = []

        for memory_type in memory_pool_types:

            jmx_data = self.parse_bean_data(jmx_result, "MemoryPool", memory_type)

            # print(json.dumps( jmx_data, indent = 2 ))

            memory_type = memory_type.replace(' ', '_').replace("'","")

            status   = jmx_data.get("status")

            if(status == 200):
                for k in [ 'init', 'committed', 'max', 'used' ]:
                    result.append({
                        "key": ".".join( [ prefix, 'MemoryPool', memory_type, k ] ),
                        "value": jmx_data.get(k)
                    })

                used      = jmx_data.get('used', 0)
                committed = jmx_data.get('committed')
                max       = jmx_data.get('max')

                if(used > 0 ):
                    percent   = ( 100 * used / committed )
                else:
                    percent   = 0

                if(max != -1):
                    percent   = ( 100 * used / max )

                result.append({
                    "key": ".".join( [ prefix, 'MemoryPool', memory_type, 'used_percent' ] ),
                    "value": round(percent, 3)
                })

        return result

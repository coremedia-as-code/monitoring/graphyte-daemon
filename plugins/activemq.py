#

from __future__ import print_function

import sys
import re
import json
import base
import time
import requests
from requests import RequestException, ConnectionError, URLRequired, TooManyRedirects, Timeout

import logging

# disable log messages from the Requests library
logging.getLogger("urllib3").setLevel(logging.CRITICAL)

class ActiveMQ(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.activemq_url      = kwargs.get("activemq_url", "http://127.0.0.1:8161/api/jolokia" )
        self.activemq_username = kwargs.get("activemq_username", None )
        self.activemq_password = kwargs.get("activemq_password", None )
        self.activemq_broker   = kwargs.get("activemq_broker", "localhost" )
        self.activemq_queue_name = kwargs.get("activemq_queue_name",  ["*"] )

    def _query_amq(self, url, auth):
        """

        """
        try:
            req = requests.get(url, auth=auth)
            req.raise_for_status()
            data = json.loads(req.text)

        except RequestException as ex:
            pp.pprint(ex)
            return {}
            # self.log.error("Apache-MQ - CRITICAL \n Could not complete request \n {}".format(ex.message))
            #sys.exit(self.ExitCode.CRITICAL.value)

        except ConnectionError as ex:
            pp.pprint(ex)
            #self.log.error("Apache-MQ - CRITICAL \n Could not connect to service \n {}".format(ex.message))
            #sys.exit(self.ExitCode.CRITICAL.value)

        except TooManyRedirects as ex:
            pp.pprint(ex)
            #self.log.error("Apache-MQ - CRITICAL \n Too many Redirects \n {}".format(ex.message))
            #sys.exit(self.ExitCode.CRITICAL.value)

        except Timeout as ex:
            pp.pprint(ex)
            #self.log.error("Apache-MQ - CRITICAL \ Connection timeout \n {}".format(ex.message))
            #sys.exit(self.ExitCode.CRITICAL.value)

        try:
            data.pop("request")
            data.pop("stacktrace")
            data.pop("error")
        except KeyError:
            pass

        return data


    def _health_status(self):
        """

        """
        result   = []
        prefix   = self.normalize_service( "activemq" )

        amq_path = "%s/read/org.apache.activemq:type=Broker,brokerName=%s" % ( self.activemq_url, self.activemq_broker )

        data = self._query_amq( amq_path, auth=(self.activemq_username, self.activemq_password) )

        status   = data.get("status")

        if(status == 200):

            values = data.get("value")

            #print(json.dumps( values, indent = 2 ))

            count_topics   = len(values.get("Topics"))
            count_topicSub = len(values.get("TopicSubscribers"))
            count_queueSub = len(values.get("QueueSubscribers"))
            count_queues   = len(values.get("Queues"))

            values.pop("Topics")
            values.pop("TopicSubscribers")
            values.pop("QueueSubscribers")
            values.pop("Queues")

            result.append({
                "key": ".".join( [ prefix, "Uptime" ] ),
                "value": values.get("UptimeMillis")
            })
            result.append({
                "key": ".".join( [ prefix, "Storage", "Used" ] ),
                "value": values.get("StorePercentUsage")
            })
            result.append({
                "key": ".".join( [ prefix, "Memory", "Used" ] ),
                "value": values.get("MemoryPercentUsage")
            })

            result.append({
                "key": ".".join( [ prefix, "Connections", "Total" ] ),
                "value": values.get("TotalConnectionsCount")
            })
            result.append({
                "key": ".".join( [ prefix, "Connections", "Current" ] ),
                "value": values.get("CurrentConnectionsCount")
            })
            result.append({
                "key": ".".join( [ prefix, "Broker", "Total", "Topics" ] ),
                "value": int(count_topics)
            })
            result.append({
                "key": ".".join( [ prefix, "Broker", "Total", "Queues" ] ),
                "value": int(count_queues)
            })

            result.append({
                "key": ".".join( [ prefix, "Broker", "Total", "TopicSubscribers" ] ),
                "value": int(count_topicSub)
            })
            result.append({
                "key": ".".join( [ prefix, "Broker", "Total", "QueueSubscribers" ] ),
                "value": int(count_queueSub)
            })

            result.append({
                "key": ".".join( [ prefix, "Broker", "Total", "Producers" ] ),
                "value": values.get("TotalProducerCount")
            })
            result.append({
                "key": ".".join( [ prefix, "Broker", "Total", "Consumers" ] ),
                "value": values.get("TotalConsumerCount")
            })
            result.append({
                "key": ".".join( [ prefix, "Broker", "Total", "Messages" ] ),
                "value": values.get("TotalMessageCount")
            })
            result.append({
                "key": ".".join( [ prefix, "Broker", "Total", "Dequeue" ] ),
                "value": values.get("TotalDequeueCount")
            })
            result.append({
                "key": ".".join( [ prefix, "Broker", "Total", "Enqueue" ] ),
                "value": values.get("TotalEnqueueCount")
            })

        return result


    def _queue_status(self):
        """

        """
        result   = []
        prefix   = self.normalize_service( "activemq" )

        amq_path = "%s/read/org.apache.activemq:type=Broker,brokerName=%s,destinationType=Queue,destinationName=%s"

        for q in self.activemq_queue_name:

            _amq_path = amq_path % ( self.activemq_url, self.activemq_broker, q )

            data = self._query_amq( _amq_path, auth=(self.activemq_username, self.activemq_password) )

            status = data.get('status')

            if(status == 200):
                values = data.get('value')
                queue_count = len(values)

                for _name, queue_value in values.items():
                  """
                  """
                  if( _name.startswith("org.apache.activemq") ):
                      name = queue_value.get('Name').replace('.>','').replace('.', '_')

                      for v in ["MemoryPercentUsage","ForwardCount","QueueSize","DispatchCount","EnqueueCount","ExpiredCount","DequeueCount"]:
                          result.append({
                              "key": ".".join( [ prefix, 'Queue', name, v.replace('QueueSize', 'pending').replace('Count', '').lower() ]),
                              "value": queue_value.get(v)
                          })
                  else:
                      name = q.replace('.>','').replace('.', '_')

                      for v in ["MemoryPercentUsage","ForwardCount","QueueSize","DispatchCount","EnqueueCount","ExpiredCount","DequeueCount"]:
                          if( v == _name ):
                              result.append({
                                  "key": ".".join( [ prefix, 'Queue', name, v.replace('QueueSize', 'pending').replace('Count', '').lower() ]),
                                  "value": queue_value
                              })
                          else:
                              continue

        return result


    def get_stats(self):
        """

        """
        result  = []

        result += self._health_status()
        result += self._queue_status()

        return result

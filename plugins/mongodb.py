#

from __future__ import print_function

import sys
import re
import json
import base
import time
import logging
import pprint

import pymongo
from pymongo import MongoClient

pp = pprint.PrettyPrinter(indent=2)

class Mongodb(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.mongodb_host     = kwargs.get("mongodb_host", "127.0.0.1" )
        self.mongodb_username = kwargs.get("mongodb_username", None )
        self.mongodb_password = kwargs.get("mongodb_password", None )
        self.mongodb_database = kwargs.get("mongodb_database", 'admin' )
        self.mongodb_authSource = kwargs.get("mongodb_authSource", 'admin' )
        self.mongodb_port     = 27017

    def _connection_to(self, host, port):
        """

        """
        if( self.mongodb_username ):
            uri = "mongodb://%s:%s@%s/%s?authSource=%s"
            uri = uri % ( self.mongodb_username, self.mongodb_password, host, self.mongodb_database, self.mongodb_authSource )
        else:
            uri = "mongodb://%s/%s"
            uri = uri % ( host, self.mongodb_database )

        client = pymongo.MongoClient( uri )

        # pp.pprint( client )

        return client

    def _set_read_preference(self, db):
        """
        """
        if(pymongo.version >= "2.1"):
            db.read_preference = pymongo.ReadPreference.SECONDARY



    def __server_status_metrics(self, server_status):
        """
        """
        result        = []

        result.append({
            "key": ".".join( [ self.prefix, 'uptime' ]),
            "value": server_status['uptime']
        })

        global_lock    = server_status.get('globalLock')
        current_queue  = global_lock.get('currentQueue')
        active_clients = global_lock.get('activeClients')
        connections    = server_status.get('connections')

        if( 'ratio' in global_lock ):
            result.append({
                "key": ".".join( [ self.prefix, 'lock', 'ratio' ]),
                "value": '%.5f' % global_lock.get('ratio')
            })


        result.append({
            "key": ".".join( [ self.prefix, 'lock', 'queue', 'total' ]),
            "value": current_queue.get('total', 0)
        })
        result.append({
            "key": ".".join( [ self.prefix, 'lock', 'queue', 'readers' ]),
            "value": current_queue.get('readers', 0)
        })
        result.append({
            "key": ".".join( [ self.prefix, 'lock', 'queue', 'writers' ]),
            "value": current_queue.get('writers', 0)
        })

        result.append({
            "key": ".".join( [ self.prefix, 'lock', 'clients', 'writers' ]),
            "value": active_clients.get('writers', 0)
        })
        result.append({
            "key": ".".join( [ self.prefix, 'lock', 'clients', 'writers' ]),
            "value": active_clients.get('writers', 0)
        })
        result.append({
            "key": ".".join( [ self.prefix, 'lock', 'clients', 'writers' ]),
            "value": active_clients.get('writers', 0)
        })

        result.append({
            "key": ".".join( [ self.prefix, 'connections', 'active' ]),
            "value": connections.get('active', 0)
        })
        result.append({
            "key": ".".join( [ self.prefix, 'connections', 'current' ]),
            "value": connections.get('current', 0)
        })
        result.append({
            "key": ".".join( [ self.prefix, 'connections', 'available' ]),
            "value": connections.get('available', 0)
        })

        result.append({
            "key": ".".join( [ self.prefix, 'mem', 'residentMb' ]),
            "value": server_status['mem']['resident']
        })
        result.append({
            "key": ".".join( [ self.prefix, 'mem', 'virtualMb' ]),
            "value": server_status['mem']['virtual']
        })

        if('indexCounters' in server_status):
            """
            """
            if( 'btree' in server_status['indexCounters']):

                result.append({
                    "key": ".".join( [ self.prefix, 'indexes', 'missRatio' ]),
                    "value": '%.5f' % server_status['indexCounters']['btree']['missRatio']
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'indexes', 'hits' ]),
                    "value": server_status['indexCounters']['btree']['hits']
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'indexes', 'misses' ]),
                    "value": server_status['indexCounters']['btree']['misses']
                })

            else:
                result.append({
                    "key": ".".join( [ self.prefix, 'indexes', 'missRatio' ]),
                    "value": '%.5f' % server_status['indexCounters']['missRatio']
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'indexes', 'hits' ]),
                    "value": server_status['indexCounters']['hits']
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'indexes', 'misses' ]),
                    "value": server_status['indexCounters']['misses']
                })


        if('cursors' in server_status):
            """
            """
            result.append({
                "key": ".".join( [ self.prefix, 'cursors', 'open' ]),
                "value": server_status['cursors']['totalOpen']
            })
            result.append({
                "key": ".".join( [ self.prefix, 'cursors', 'timedOut' ]),
                "value": server_status['cursors']['timedOut']
            })


        if('asserts' in server_status):
            """
            """
            for assert_type, value in server_status['asserts'].items():

                result.append({
                    "key": ".".join( [ self.prefix, 'asserts', assert_type ]),
                    "value": value
                })

        return result


    def __query_performance(self, server_status):
        """
        """
        result =  []

        def query_rate(current_counter_value, now, last_count_data):
            """

            """
            def rate(count2, count1, t2, t1):
                return float(count2 - count1) / float(t2 - t1)

            counts_per_second = 0
            try:
                counts_per_second = rate(
                    current_counter_value, last_count_data[
                        'data'][query_type]['count'],
                    now, last_count_data['data'][query_type]['ts'])
            except KeyError:
                # since it is the first run
                pass
            except TypeError:
                # since it is the first
                pass
            return counts_per_second


        try:
            now = int(time.time())
            db  = self._connection.local

            self._set_read_preference(db)

            # pp.pprint( self._connection )
            # pp.pprint( db )

            for query_type in 'insert', 'query', 'update', 'delete':

                current_counter_value = server_status.get('opcounters').get( query_type )

                pp.pprint( current_counter_value )

                # use collection
                last_count = db.nagios_check.find_one( {'check': 'query_counts'} )

                pp.pprint( last_count )

                if last_count:
                    counts_per_second = query_rate(current_counter_value, now, last_count)

                    db.nagios_check.update(
                      {
                        u'_id': last_count['_id']
                      },
                      {
                        '$set': {
                          "data.%s" % query_type: {
                            'count': current_counter_value, 'ts': now
                          }
                        }
                      },
                      upsert=True
                    )

                else:
                    counts_per_second = 0

                    db.nagios_check.insert(
                      {
                        'check': 'query_counts',
                        'data': {
                            query_type: {
                              'count': current_counter_value,
                              'ts': now
                            }
                          }
                      }
                    )

                result.append({
                    "key": ".".join( [ self.prefix, 'opcounters', query_type, 'perSeconds' ]),
                    "value": round(counts_per_second, 3)
                })


        except Exception as e:
            self.logerror( "Couldn't retrieve/write query performance data: {}".format(e) )

        return result


    def __heap__metrics(self, server_status):
        """
        """
        result   = []
        tcmalloc = server_status['tcmalloc']

        generic = tcmalloc.get('generic')

        heap_size  = generic.get('heap_size')
        heap_used  = generic.get('current_allocated_bytes')
        percent    = ( 100 * heap_used / heap_size )

        result.append({
            "key": ".".join( [ self.prefix, 'memory', 'heap', 'size' ]),
            "value": heap_size
        })
        result.append({
            "key": ".".join( [ self.prefix, 'memory', 'heap', 'used' ]),
            "value": heap_used
        })
        result.append({
            "key": ".".join( [ self.prefix, 'memory', 'heap', 'used_percent' ]),
            "value": round(percent, 3)
        })

        return result


    def __network__metrics(self, server_status):
        """
        """
        result  = []
        network = server_status['network']

        result.append({
            "key": ".".join( [ self.prefix, 'network', 'bytes', 'tx' ]),
            "value": network.get('bytesOut')
        })
        result.append({
            "key": ".".join( [ self.prefix, 'network', 'bytes', 'rx' ]),
            "value": network.get('bytesIn')
        })
        result.append({
            "key": ".".join( [ self.prefix, 'network', 'requests', 'total' ]),
            "value": network.get('numRequests')
        })

        return result


    def __commands__metrics(self, server_status):
        """
        """
        result        = []
        commands      = server_status['metrics']['commands']

        #pp.pprint(commands)

        for m in ['authenticate','buildInfo','createIndexes','delete','drop','find','findAndModify','insert','listCollections','mapReduce','renameCollection','update']:

            result.append({
                "key": ".".join( [ self.prefix, 'commands', m, 'total' ]),
                "value": commands[m]['total']
            })
            result.append({
                "key": ".".join( [ self.prefix, 'commands', m, 'failed' ]),
                "value": commands[m]['failed']
            })

        return result


    def __storage_engine(self, server_status):
        """
        """
        result        = []
        #pp.pprint(server_status)

        storage_engine_name = server_status['storageEngine']['name']
        storage_engine      = server_status[storage_engine_name]

        if(storage_engine):

            storage_transactions = storage_engine['concurrentTransactions']
            storage_cache        = storage_engine['cache']
            storage_connection   = storage_engine['connection']
            storage_block_manager = storage_engine['block-manager']

            # pp.pprint(storage_block_manager)

            if(storage_block_manager):
                """
                """
                storage_bytes_read            = storage_block_manager.get('bytes read')
                storage_bytes_written         = storage_block_manager.get('bytes written')
                storage_blocks_read           = storage_block_manager.get('blocks read')
                storage_blocks_written        = storage_block_manager.get('blocks written')

                result.append({
                    "key": ".".join( [ self.prefix, 'storage-engine', storage_engine_name, 'block-manager', 'bytes', 'rx' ]),
                    "value": storage_bytes_read
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'storage-engine', storage_engine_name, 'block-manager', 'bytes', 'tx' ]),
                    "value": storage_bytes_written
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'storage-engine', storage_engine_name, 'block-manager', 'blocks', 'rx' ]),
                    "value": storage_blocks_read
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'storage-engine', storage_engine_name, 'block-manager', 'blocks', 'tx' ]),
                    "value": storage_blocks_written
                })


            if(storage_connection):
                """
                """
                storage_connection_io_read    = storage_connection.get('total read I/Os')
                storage_connection_io_write   = storage_connection.get('total write I/Os')
                storage_connection_files_open = storage_connection.get('files currently open')

                result.append({
                    "key": ".".join( [ self.prefix, 'storage-engine', storage_engine_name, 'connection', 'io', 'read', 'total' ]),
                    "value": storage_connection_io_read
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'storage-engine', storage_engine_name, 'connection', 'io', 'write', 'total' ]),
                    "value": storage_connection_io_write
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'storage-engine', storage_engine_name, 'connection', 'files', 'open' ]),
                    "value": storage_connection_files_open
                })


            if(storage_transactions):
                """
                """
                read_out          = storage_transactions['read']['out']
                read_available    = storage_transactions['read']['available']
                write_out         = storage_transactions['write']['out']
                write_available   = storage_transactions['write']['available']

                result.append({
                    "key": ".".join( [ self.prefix, 'wiredTiger', 'concurrentTransactions', 'read', 'out'  ]),
                    "value": read_out
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'wiredTiger', 'concurrentTransactions', 'read', 'available' ]),
                    "value": read_available
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'wiredTiger', 'concurrentTransactions', 'write', 'out' ]),
                    "value": write_out
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'wiredTiger', 'concurrentTransactions', 'write', 'available' ]),
                    "value": write_available
                })


            if(storage_cache):
                """
                """
                bytes         = storage_cache.get('bytes currently in the cache')
                tracked       = storage_cache.get('tracked dirty bytes in the cache')
                maximum       = storage_cache.get('maximum bytes configured')
                modified      = storage_cache.get('modified pages evicted')
                unmodified    = storage_cache.get('unmodified pages evicted')

                result.append({
                    "key": ".".join( [ self.prefix, 'wiredTiger', 'cache', 'in-cache', 'bytes' ]),
                    "value": bytes
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'wiredTiger', 'cache', 'in-cache', 'tracked-dirty' ]),
                    "value": tracked
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'wiredTiger', 'cache', 'configured', 'max-bytes' ]),
                    "value": maximum
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'wiredTiger', 'cache', 'evicted-pages', 'modified' ]),
                    "value": modified
                })
                result.append({
                    "key": ".".join( [ self.prefix, 'wiredTiger', 'cache', 'evicted-pages', 'unmodified' ]),
                    "value": unmodified
                })

        return result


    def __oplog_stats(self):
        """
        """
        oplogStats = dict()

        # print(self._connection.list_database_names() )
        db = self._connection.test
        pp.pprint( db.command("serverStatus"))

        try:
            db = self._connection.test
            #print(type(db))


            if db.system.namespaces.find_one({"name": "local.oplog.rs"}):
                oplog = "oplog.rs"
            elif db.system.namespaces.find_one({"name": "local.oplog.$main"}):
                oplog = "oplog.$main"
            else:
                return

            self._set_read_preference(self._connection['admin'])
            data = self._connection['local'].command(
                pymongo.son_manipulator.SON([('collstats', oplog)]))

            oplog_size = data['size']
            oplog_storage_size = data['storageSize']
            oplog_used_storage = int(
                float(oplog_size) / oplog_storage_size * 100 + 1)
            oplog_collection = self._connection['local'][oplog]
            first_item = oplog_collection.find().sort(
                "$natural", pymongo.ASCENDING).limit(1)[0]['ts']
            last_item = oplog_collection.find().sort(
                "$natural", pymongo.DESCENDING).limit(1)[0]['ts']
            time_in_oplog = (
                last_item.as_datetime() - first_item.as_datetime())

            try:  # work starting from python2.7
                hours_in_oplog = time_in_oplog.total_seconds() / 60 / 60
            except:
                hours_in_oplog = float(
                    time_in_oplog.seconds + time_in_oplog.days * 24 * 3600) / 60 / 60

            approx_hours_in_oplog = hours_in_oplog * 100 / oplog_used_storage

            oplogStats['oplog.size'] = oplog_size
            oplogStats['oplog.storageSize'] = oplog_storage_size
            oplogStats['oplog.usedStoragePercentage'] = oplog_used_storage
            oplogStats['oplog.hoursInOplog'] = hours_in_oplog
            oplogStats['oplog.approxHoursInOplog'] = approx_hours_in_oplog

        finally:
            return oplogStats


    def __page_fault__metrics(self, server_status):
        """
        """
        result        = []
        delta_time    = 1  # second

        extra_info    = server_status.get('extra_info')

        try:
            page_faults1 = extra_info.get('page_faults')
            time.sleep(delta_time)
            page_faults2 = extra_info.get('page_faults')

            try:
                result.append({
                    "key": ".".join( [ self.prefix, 'mem', 'pageFault', 'rate' ]),
                    "value": (int(page_faults2) - int(page_faults1)) / delta_time
                })

            except KeyError:
                self.logwarning( "WARNING - Can't get extra_info.page_faults counter from MongoDB")

        except Exception as e:
            self.logerror( "ERROR: Couldn't determine page fault rate:", e )

        return result


    def get_stats(self):
        """

        """
        result         = []
        self.prefix    = self.normalize_service('mongodb')

        try:
            self._connection = self._connection_to(self.mongodb_host, self.mongodb_port)
        except ConnectionError as e:
            self.logerror(e)

        # if( self.mongodb_username ):
        #
        #   try:
        #       self._connection.admin.authenticate(self.mongodb_username, self.mongodb_password)
        #   except pymongo.errors.InvalidOperation as error:
        #       print("Could not authenticate at mongodb: {}".format(error))
        #       return result
        #   except pymongo.errors.OperationFailure as error:
        #       print("Could not authenticate at mongodb: {}".format(error))
        #       return result
        #   except pymongo.errors.PyMongoError as error:
        #       print("Could not authenticate at mongodb: {}".format(error))
        #       return result


          # if( not self._connection.admin.authenticate(self.mongodb_username, self.mongodb_password) ):
          #   print("Could not authenticate at mongodb")
          #   return result

        server_status = self._connection.test.command("serverStatus")

        result += self.__query_performance(server_status)

        result += self.__server_status_metrics(server_status)
        result += self.__network__metrics(server_status)
        result += self.__page_fault__metrics(server_status)
        result += self.__commands__metrics(server_status)
        result += self.__heap__metrics(server_status)
        result += self.__storage_engine(server_status)

        return result


# https://gist.github.com/thpham/9060170
# https://salsa.debian.org/debian/netdata/blob/master/collectors/python.d.plugin/mongodb/mongodb.chart.py
# https://gist.github.com/mieitza/c1ad5fe535209928b00d

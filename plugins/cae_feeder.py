#

import sys
import re
import json
import base

class CAEFeeder(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def get_stats(self):
        """

        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "caefeeder-preview",
            "caefeeder-live"]

        valid, application = self._validate_application( valid_applications,  self.jmx_application )

        if( valid == False ):
            self.logwarning( "%s is no valid application for CAEFeeder" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

        data = {
            "Health": {
              "mbean": 'com.coremedia:type=Health,application=%s' % (self.jmx_application),
              "attribute": [
                'Healthy',
                'MaximumHeartBeat']
            },
            "ProactiveEngine": {
              "mbean": 'com.coremedia:type=ProactiveEngine,application=%s' % (self.jmx_application),
              "attribute": [
                'HeartBeat',
                'ValuesCount',
                'KeysCount',
                'Uptime',
                'QueueProcessedPerSecond',
                'SendSuccessTimeLatest']
            }
        }

        if( self.spring_boot ):
            data["Health"]["mbean"]           = "com.coremedia:type=Health,application=coremedia"
            data["ProactiveEngine"]["mbean"]  = "com.coremedia:type=ProactiveEngine,application=coremedia"

        jmx_result                = self.read_jmx_data(data)
        # print(json.dumps( jmx_result, indent = 2 ))
        jmx_health_data           = self.parse_bean_data(jmx_result, "Health")
        jmx_proactive_engine_data = self.parse_bean_data(jmx_result, "ProactiveEngine")
        #print(json.dumps( jmx_data, indent = 2 ))
        health_status             = jmx_health_data.get("status", 404)
        proactive_engine_status   = jmx_proactive_engine_data.get("status", 404)

        if(health_status == 200):

            healthy   = jmx_health_data.get("Healthy", False)
            healthy   = 1 if healthy else 0

            result.append({
                "key": ".".join( [ prefix, 'Health', 'healthy' ]),
                "value": healthy
            })

        if(proactive_engine_status == 200):
            # defaults
            max_entries     = 0  # (KeysCount) Number of (active) keys
            current_entries = 0  # (ValuesCount) Number of (valid) values. It is less or equal to 'keysCount'
            diff_entries    = 0  #
            invalidations   = 0  # (InvalidationCount) Number of invalidations which have been received
            heartbeat       = 0  # (HeartBeat) The heartbeat of this service: Milliseconds between now and the latest activity. A low value indicates that the service is alive. An constantly increasing value might be caused by a 'sick' or dead service
            queue_capacity  = 0  # (QueueCapacity) The queue's capacity: Maximum number of items which can be enqueued
            queue_max_size  = 0  # (QueueMaxSize) Maximum number of items which had been waiting in the queue
            queue_size      = 0  # (QueueSize) Number of items waiting in the queue for being processed. Less or equal than 'queue_capacity'. Zero means that ProactiveEngine is idle.

            max_entries     = jmx_proactive_engine_data.get('KeysCount', 0)
            current_entries = jmx_proactive_engine_data.get('ValuesCount', 0)
            diff_entries    = int( max_entries - current_entries )
            invalidations   = jmx_proactive_engine_data.get('InvalidationCount', 0)
            heartbeat       = jmx_proactive_engine_data.get('HeartBeat', 0)
            queue_capacity  = jmx_proactive_engine_data.get('QueueCapacity', 0)
            queue_max_size  = jmx_proactive_engine_data.get('QueueMaxSize', 0)
            queue_size      = jmx_proactive_engine_data.get('QueueSize', 0)

            result.append({
                "key": ".".join( [ prefix, 'ProactiveEngine', 'feeder', 'entries', 'max' ]),
                "value": max_entries
            })
            result.append({
                "key": ".".join( [ prefix, 'ProactiveEngine', 'feeder', 'entries', 'current' ]),
                "value": current_entries
            })
            result.append({
                "key": ".".join( [ prefix, 'ProactiveEngine', 'feeder', 'entries', 'diff' ]),
                "value": diff_entries
            })
            result.append({
                "key": ".".join( [ prefix, 'ProactiveEngine', 'feeder', 'invalidations' ]),
                "value": invalidations
            })
            result.append({
                "key": ".".join( [ prefix, 'ProactiveEngine', 'feeder', 'heartbeat' ]),
                "value": heartbeat
            })
            result.append({
                "key": ".".join( [ prefix, 'ProactiveEngine', 'queue', 'capacity' ]),
                "value": queue_capacity
            })
            result.append({
                "key": ".".join( [ prefix, 'ProactiveEngine', 'queue', 'max_waiting' ]),
                "value": queue_max_size
            })
            result.append({
                "key": ".".join( [ prefix, 'ProactiveEngine', 'queue', 'waiting' ]),
                "value": queue_size
            })

        return result

#

import sys
import re
import json
import base

class GarbageCollector(base.Base):
    """

    """
    def __init__(self, *args, **kwargs):

        base.Base.__init__(self)

        self.jmx_port        = kwargs.get("jmx_port", 0)
        self.jmx_application = kwargs.get("jmx_application", '')
        self.jolokia_url     = kwargs.get("jolokia_url", "http://localhost:8080/jolokia/" )
        self.spring_boot     = kwargs.get("spring_boot", True)

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()

    def __parse_gc(self, data, prefix, gc):
        """
        """
        result = []

        gc = gc.replace(' ','-')
        #print(gc)
        #print(json.dumps( data, indent = 2 ))

        data  = data.get("LastGcInfo", None)

        if( data != None ):

            thread_count  = data.get('GcThreadCount')   # The number of GC threads.
            duration      = data.get('duration')        # The elapsed time of this GC. (milliseconds)

            result.append({
                "key": ".".join( [ prefix, 'GarbageCollector', gc, 'threads', 'count' ]),
                "value": thread_count
            })
            result.append({
                "key": ".".join( [ prefix, 'GarbageCollector', gc, 'duration', 'time' ]),
                "value": duration
            })

        return result




    def get_stats(self):
        """

        """
        result   = []

        avail  = self.jolokia_availability()
        status = avail.get('status')

        if( int(status) == 500 ):
            return result

        valid_applications = [
            "content-management-server",
            "master-live-server",
            "replication-live-server",
            "workflow-server",
            "content-feeder",
            "user-changes",
            "elastic-worker",
            "caefeeder-preview",
            "caefeeder-live",
            "studio",
            "solr",
            "solr-master",
            "solr-slave",
            "sitemanager",
            "headless-server",
            "eds",
            "cae-preview",
            "cae-live-1",
            "cae-live-2",
            "cae-live-3",
            "cae-live-4",
            "cae-live-5",
            "cae-live-6",
            "cae-live-7",
            "cae-live-8",
            "cae-live-9",
            "cae-live"]

        valid, application = self._validate_application(valid_applications, self.jmx_application)

        if( valid == False ):
            self.logwarning( "%s is no valid application for GarbageCollector" %(self.jmx_application) )
            return []

        prefix   = self.normalize_service(self.jmx_application)

        if( self.spring_boot ):
            """
            """
            data = {
              "all": {
                "mbean": "java.lang:type=GarbageCollector,name=*",
                "attribute": [
                  "CollectionCount",
                  "Valid",
                  "CollectionTime",
                  "LastGcInfo"]
              }
            }

            jmx_result    = self.read_jmx_data(data)
            jmx_value     = jmx_result[0].get('value')

            for k,v in jmx_value.items():
                if( k.startswith("java.lang:name=")):
                    # java.lang:name=PS Scavenge,type=GarbageCollector
                    gc_type = ''
                    pattern = (r'^java.lang:name=(?P<gc_type>.+?),type=GarbageCollector')
                    regex   = re.compile(pattern)
                    m       = regex.match(k)
                    k       = m.groupdict() if m is not None else None
                    gc_type = k.get('gc_type')

                    result += self.__parse_gc( v , prefix, gc_type )

            return result

        data = {
            "GCParNew": {
              "mbean": "java.lang:type=GarbageCollector,name=ParNew",
              "attribute": [
                "CollectionCount",
                "Valid",
                "CollectionTime",
                "LastGcInfo"]
            },
            "GCConcurrentMarkSweep": {
              "mbean": "java.lang:type=GarbageCollector,name=ConcurrentMarkSweep",
              "attribute": [
                "CollectionCount",
                "Valid",
                "CollectionTime",
                "LastGcInfo"]
            }
        }

        jmx_result         = self.read_jmx_data(data)

        # print(json.dumps( jmx_result, indent = 2 ))

        jmx_gc_parnew_data    = self.parse_bean_data(jmx_result, "GarbageCollector", "ParNew")
        jmx_gc_marksweep_data = self.parse_bean_data(jmx_result, "GarbageCollector", "ConcurrentMarkSweep")

        parnew_status         = jmx_gc_parnew_data.get("status")
        marksweep_status      = jmx_gc_marksweep_data.get("status")

        if(parnew_status == 200):

            jmx_gc_parnew_data  = jmx_gc_parnew_data.get("LastGcInfo")

            thread_count  = jmx_gc_parnew_data.get('GcThreadCount')   # The number of GC threads.
            duration      = jmx_gc_parnew_data.get('duration')        # The elapsed time of this GC. (milliseconds)

            result.append({
                "key": ".".join( [ prefix, 'GarbageCollector', 'ParNew', 'threads', 'count' ]),
                "value": thread_count
            })
            result.append({
                "key": ".".join( [ prefix, 'GarbageCollector', 'ParNew', 'duration', 'time' ]),
                "value": duration
            })

        if(marksweep_status == 200):

            jmx_gc_marksweep_data  = jmx_gc_marksweep_data.get("LastGcInfo")

            thread_count  = jmx_gc_marksweep_data.get('GcThreadCount')   # The number of GC threads.
            duration      = jmx_gc_marksweep_data.get('duration')        # The elapsed time of this GC. (milliseconds)

            result.append({
                "key": ".".join( [ prefix, 'GarbageCollector', 'ConcurrentMarkSweep', 'threads', 'count' ]),
                "value": thread_count
            })
            result.append({
                "key": ".".join( [ prefix, 'GarbageCollector', 'ConcurrentMarkSweep', 'duration', 'time' ]),
                "value": duration
            })



        return result

